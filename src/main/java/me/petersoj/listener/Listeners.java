package me.petersoj.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.petersoj.BlockOpsTDM;
import me.petersoj.game.TDMController;

public class Listeners implements Listener {
	
	private TDMController tdmController;
	
	public Listeners(TDMController tdmController){
		this.tdmController = tdmController;
		
		Bukkit.getPluginManager().registerEvents(this, BlockOpsTDM.getInstance());
	}
	
	
	
	//
	// On Join And On Quit
	//
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		if(tdmController.isGameInSession()){
			tdmController.getGameController().onJoin(e);
		}else{
			tdmController.getLobbyController().onJoin(e);
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if(tdmController.isGameInSession()){
			tdmController.getGameController().onQuit(e);
		}else{
			tdmController.getLobbyController().onQuit(e);
		}
	}
	
	@EventHandler
	public void onCmd(PlayerCommandPreprocessEvent e){
		tdmController.getPlayerController().getPlayerData(e.getPlayer().getUniqueId()).incrementCoins(50);
	}
	
	
	/*
	
	public Listeners(){
		Bukkit.getPluginManager().registerEvents(this, BlockOps.getInstance());
	}
	
	
	//
	// On Join And On Quit
	//
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		// pull from redis
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		// push to redis
		// clear player data
	}
	
	// 
	// Gun Class Events
	//
	
	@EventHandler
	public void onSprint(PlayerToggleSprintEvent e){
		PlayerData boPlayer = PlayerData.getBlockOpsPlayer(e.getPlayer());
		if(boPlayer != null && boPlayer.selectedClass != null){
			boPlayer.selectedClass.onToggleSprint(e, boPlayer);
		}
	}
	
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e){
		e.setCancelled(true);
		PlayerData boPlayer = PlayerData.getBlockOpsPlayer(e.getPlayer());
		if(boPlayer != null && boPlayer.selectedClass != null){
			boPlayer.selectedClass.onToggleSneak(e, boPlayer);
		}
	}
	
	@EventHandler
	public void onDropItem(PlayerDropItemEvent e){
		PlayerData boPlayer = PlayerData.getBlockOpsPlayer(e.getPlayer());
		if(boPlayer != null && boPlayer.selectedClass != null){
			boPlayer.selectedClass.onDropItem(e, boPlayer);
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		// TODO Other items they click like chest(Create a Class) and menu (compass)
		PlayerData boPlayer = PlayerData.getBlockOpsPlayer(e.getPlayer());
		if(boPlayer != null && boPlayer.selectedClass != null){
			boPlayer.selectedClass.onInteract(e, boPlayer);
		}
	}
	
	@EventHandler
	public void onHandItemChange(PlayerItemHeldEvent e){
		
	}
	
	
	//
	// Other events
	//
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(PlayerData.getBlockOpsPlayer(e.getPlayer()).rank != Rank.OWNER){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(PlayerData.getBlockOpsPlayer(e.getPlayer()).rank != Rank.OWNER){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent e){
		// pick up gun, if it is a gun
	}
	
	@EventHandler
	public void onSwapHandItems(PlayerSwapHandItemsEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		if(PlayerData.getBlockOpsPlayer((Player) e.getWhoClicked()).rank != Rank.OWNER){
			e.setCancelled(true);
		}
	}
	 */
		
}
