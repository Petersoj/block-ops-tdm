package me.petersoj.view;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.petersoj.util.Utils;

public enum ItemBorder {
	
	// TODO Proper materials
	// Don't do predicate values for these because they cause a massive drop in frames.
	
	LEFT(Material.FIRE), RIGHT(Material.FIRE),
	TOP(Material.FIRE), BOTTOM(Material.FIRE),
	LEFT_RIGHT(Material.FIRE), TOP_BOTTOM(Material.FIRE);

	private ItemStack itemStack;
	
	private ItemBorder(Material borderMaterial){
		itemStack = new ItemStack(borderMaterial);
		Utils.setItemStackBlank(itemStack);
	}
	
	public ItemStack getItemStack(){
		return itemStack;
	}
	
}
