package me.petersoj.view.hud;

import me.petersoj.player.GamePlayer;
import net.minecraft.server.v1_11_R1.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_11_R1.PacketPlayOutWorldBorder.EnumWorldBorderAction;
import net.minecraft.server.v1_11_R1.WorldBorder;

public class RedTint {

	private GamePlayer gamePlayer;
	private WorldBorder border;
	private static int[] damages = {1000,890,700,590,480,370,290,250,220,200,190,180,170,160,150,140,130,120,110,100};
	
	public RedTint(GamePlayer gamePlayer){
		this.gamePlayer = gamePlayer;
		border = new WorldBorder();
		this.resetTint(20);
	}
	
	public void setTint(int hearts){
		if(hearts <= 0){
			border.setWarningDistance(damages[hearts]*1000);
		}else if(hearts <= 20){
			border.setWarningDistance(damages[hearts-1]*1000);
		}
		PacketPlayOutWorldBorder packet = new PacketPlayOutWorldBorder(border, EnumWorldBorderAction.SET_WARNING_BLOCKS);
		gamePlayer.sendPacket(packet);
	}
	
	public void resetTint(int hearts){
		border.setCenter(gamePlayer.getPlayer().getLocation().getX(), gamePlayer.getPlayer().getLocation().getZ());
		if(hearts <= 0){
			border.setWarningDistance(damages[hearts]*1000);
		}else if(hearts <= 20){
			border.setWarningDistance(damages[hearts-1]*1000);
		}
		border.setWarningTime(15);
		border.setSize(200000);
		PacketPlayOutWorldBorder packet = new PacketPlayOutWorldBorder(border, EnumWorldBorderAction.INITIALIZE);
		gamePlayer.sendPacket(packet);
	}
}
