package me.petersoj.view.hud;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import me.petersoj.BlockOpsTDM;
import me.petersoj.player.BlockOpsPlayer;
import me.petersoj.player.GamePlayer;
import me.petersoj.weapon.gun.Gun;

public class HudController {

	private BlockOpsPlayer boPlayer;
	
	private RedTint redTint;
	private ScoreBoard scoreBoard;
	private Titles titles;
	
	private ArrayList<BukkitTask> damageIndicators;
	private BukkitTask hudBarTask;
	
	private ItemStack hudBarItemStack;

	
	public HudController(BlockOpsPlayer boPlayer){
		this.boPlayer = boPlayer;
		
		this.scoreBoard = new ScoreBoard(boPlayer);
		this.titles = new Titles(boPlayer);
		
		if(boPlayer instanceof GamePlayer){
			this.redTint = new RedTint((GamePlayer) boPlayer);
			this.damageIndicators = new ArrayList<BukkitTask>();
			
			this.hudBarItemStack = new ItemStack(Material.FIRE); // TODO Accurate value
		}
	}
	
	public void createDamageIndicator(GamePlayer enemy){
		Vector playerDirection = this.boPlayer.getPlayer().getLocation().getDirection();
		Vector betweenEnemy = this.boPlayer.getPlayer().getEyeLocation().toVector().subtract(enemy.getPlayer().getEyeLocation().toVector());
		BukkitTask task = new BukkitRunnable() {
			@Override
			public void run() {
				// TODO BUG TEST 
				float angle = (float) Math.toDegrees(Math.atan2(playerDirection.getZ(), playerDirection.getX()) 
						- Math.atan2(betweenEnemy.getZ(), betweenEnemy.getX()));
				if (angle < 0) {
		            angle += 360.0F;
		        }
				
				// Do angle checks
			}
		}.runTaskTimer(BlockOpsTDM.getInstance(), 0, 7);
		this.damageIndicators.add(task);
	}
	
	public void cancelAllDamageIndicators(){
		this.titles.sendTitle("", "", 0, 0, 0);
		for(BukkitTask task : this.damageIndicators){
			task.cancel();
		}
	}
	
	public void startHudBarUpdateTask(){
		if(this.boPlayer instanceof GamePlayer){
			GamePlayer gamePlayer = (GamePlayer) this.boPlayer;
			
			this.hudBarTask = new BukkitRunnable() {
				@Override
				public void run() {
					if(gamePlayer.getCurrentWeapon() instanceof Gun){
						Gun gun = (Gun) gamePlayer.getCurrentWeapon();
						String hudBarString = gun.getHudBarSpacingString() + ChatColor.WHITE;
						for(int index = 0; index < gun.getBullets(); index++){
							hudBarString += "|";
						}
						hudBarString +=  " " + gun.getName();
						titles.sendActionBar(hudBarString);
					}
				}
			}.runTaskTimer(BlockOpsTDM.getInstance(), 0, 30); // every 1.5 seconds
		}
	}
	
	public void cancelHudBarUpdateTask(){
		this.titles.sendActionBar("");
		hudBarTask.cancel();
	}


	public RedTint getRedTint() {
		return redTint;
	}

	public ScoreBoard getScoreBoard() {
		return scoreBoard;
	}

	public Titles getTitles() {
		return titles;
	}

	public ItemStack getHudBarItemStack() {
		return hudBarItemStack;
	}

	
}
