package me.petersoj.view.hud;

import java.lang.reflect.Field;

import me.petersoj.player.BlockOpsPlayer;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_11_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_11_R1.PacketPlayOutTitle.EnumTitleAction;

public class Titles {
	
	private BlockOpsPlayer boPlayer;
	
	public Titles(BlockOpsPlayer boPlayer){
		this.boPlayer = boPlayer;
	}
	
	public void sendTitle(String title, String subtitle, int fadeIn, int showTime, int fadeOut){
		this.sendOneTitle(EnumTitleAction.TITLE, title, fadeIn, showTime, fadeOut);
		this.sendOneTitle(EnumTitleAction.SUBTITLE, subtitle, fadeIn, showTime, fadeOut);
	}
	private void sendOneTitle(EnumTitleAction titleType, String text, int fadeInTime, int showTime, int fadeOutTime) {
		IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + text + "\"}");
		if(titleType != EnumTitleAction.TITLE && titleType != EnumTitleAction.SUBTITLE) return;
		PacketPlayOutTitle title = new PacketPlayOutTitle(titleType, chatTitle);
		PacketPlayOutTitle length = new PacketPlayOutTitle(fadeInTime, showTime ,fadeOutTime);
		boPlayer.sendPacket(title);
		boPlayer.sendPacket(length);
    }
	
	public void sendActionBar(String text){
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\":\"" + text + "\"}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbc, (byte) 2);
		boPlayer.sendPacket(packet);
	}
	
	public void sendHeaderAndFooter(String header, String footer) {
        IChatBaseComponent chatHeader = ChatSerializer.a("{\"text\": \"" + header + "\"}");
        IChatBaseComponent chatFooter = ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter headerFooterPacket = new PacketPlayOutPlayerListHeaderFooter(chatHeader);
        try {
            Field footerField = headerFooterPacket.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(headerFooterPacket, chatFooter);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        }
        boPlayer.sendPacket(headerFooterPacket);
	}
}
