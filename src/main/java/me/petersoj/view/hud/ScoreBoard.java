package me.petersoj.view.hud;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

import me.petersoj.data.Rank;
import me.petersoj.player.BlockOpsPlayer;
import me.petersoj.player.LobbyPlayer;

public class ScoreBoard{
	
	private Scoreboard scoreboard;
	
	public ScoreBoard(BlockOpsPlayer boPlayer){
		this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		
		boPlayer.getPlayer().setCollidable(false); // Might not work :(
		
		Objective objective = scoreboard.registerNewObjective("BlockOps", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "BLOCK OPS");
		
		if(boPlayer instanceof LobbyPlayer){
			// DEFAULT, PLASMA, YOUTUBE, MOD, MANAGER, OWNER
			Team defaultRank = scoreboard.registerNewTeam(Rank.DEFAULT.getTeamName());
			defaultRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			
			Team plasmaRank = scoreboard.registerNewTeam(Rank.PLASMA.getTeamName());
			plasmaRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			plasmaRank.setPrefix(ChatColor.GOLD + "" + ChatColor.BOLD + "PLASMA ");
			
			Team youtubeRank = scoreboard.registerNewTeam(Rank.YOUTUBE.getTeamName());
			youtubeRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			youtubeRank.setPrefix(ChatColor.RED + "" + ChatColor.BOLD + "YOUTUBE ");

			Team modRank = scoreboard.registerNewTeam(Rank.MOD.getTeamName());
			modRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			modRank.setPrefix(ChatColor.GREEN + "" + ChatColor.BOLD + "MOD ");
			
			Team managerRank = scoreboard.registerNewTeam(Rank.MANAGER.getTeamName());
			managerRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			managerRank.setPrefix(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "MANAGER ");
			
			Team ownerRank = scoreboard.registerNewTeam(Rank.OWNER.getTeamName());
			ownerRank.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			ownerRank.setPrefix(ChatColor.DARK_RED + "" + ChatColor.BOLD + "OWNER ");
			
			this.scoreboard.getTeam(boPlayer.getPlayerData().getRank().getTeamName()).addEntry(boPlayer.getPlayer().getName());
		}else{ // GamePlayer, SpectatorPlayer
			Team blueTeam = scoreboard.registerNewTeam("blueTeam");
			blueTeam.setAllowFriendlyFire(false);
			blueTeam.setCanSeeFriendlyInvisibles(false);
			blueTeam.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.ALWAYS);
			blueTeam.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			blueTeam.setPrefix(ChatColor.BLUE.toString());
			
			Team redTeam = scoreboard.registerNewTeam("redTeam");
			redTeam.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.ALWAYS);
			redTeam.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
			redTeam.setPrefix(ChatColor.RED.toString());
			
			Team redTeamHidden = scoreboard.registerNewTeam("redTeamHidden");
			redTeamHidden.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.NEVER);
			redTeamHidden.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);

			blueTeam.addEntry(boPlayer.getPlayer().getName());
		}
		boPlayer.getPlayer().setScoreboard(scoreboard);
	}
	
	public void setEnemyNameTagHidden(String playerName){
		Team redTeamHidden = this.scoreboard.getTeam("redTeamhidden");
		if(!redTeamHidden.hasEntry(playerName)){
			this.scoreboard.getTeam("redTeam").removeEntry(playerName);
			redTeamHidden.addEntry(playerName);
		}
	}
	
	public void setEnemyNameTagShown(String playerName){
		Team redTeam = this.scoreboard.getTeam("redTeam");
		if(!redTeam.hasEntry(playerName)){
			this.scoreboard.getTeam("redTeamHidden").removeEntry(playerName);
			redTeam.addEntry(playerName);
		}
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}
	
	public void setPlayerScoreBoardLine(String text, int row){
		for (String entry : scoreboard.getEntries()) {
			if (scoreboard.getObjective(DisplaySlot.SIDEBAR).getScore(entry).getScore() == row) {
				scoreboard.resetScores(entry);
				break;
			}
		}
		scoreboard.getObjective(DisplaySlot.SIDEBAR).getScore(text).setScore(row);
	}
}
