package me.petersoj.view.views;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;

public class MenuView extends View {
	
	private ItemStack createAClass;
	private ItemStack barracks;
	private ItemStack goToWebsite;

	public MenuView(ViewController viewController) {
		super(viewController, ChatColor.GREEN + "Menu", 3, false);
		barracks = new ItemStack(Material.BOOK);
		createAClass = new ItemStack(Material.FIRE, 1, (short) 4); // TODO First Class ItemStack
		goToWebsite = new ItemStack(Material.EMERALD_BLOCK);
	}
	
	@Override
	public void createLores(){
		Utils.setNameAndLore(createAClass, ChatColor.GREEN + "" + ChatColor.BOLD + "Create A Class", null);
		Utils.setNameAndLore(barracks, ChatColor.GOLD + "" + ChatColor.BOLD + "Barracks", null);
		
		ArrayList<String> goToStoreLore = new ArrayList<String>();
		goToStoreLore.add("");
		goToStoreLore.add(ChatColor.WHITE + "Click to visit our website where");
		goToStoreLore.add(ChatColor.WHITE + "you can view updates, post on");
		goToStoreLore.add(ChatColor.WHITE + "the forums, buy awesome ");
		goToStoreLore.add(ChatColor.WHITE + "in-game items, and much more!");
		Utils.setNameAndLore(goToWebsite, ChatColor.YELLOW + "" + ChatColor.BOLD + "Go to Store", goToStoreLore);
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.BOTTOM, 2);
		super.setItemBorder(ItemBorder.BOTTOM, 6);
		super.setItemBorder(ItemBorder.RIGHT, 7);
		super.setItem(goToWebsite, 8);
		super.setItemBorder(ItemBorder.RIGHT, 10);
		super.setItem(createAClass, 11);
		super.setItemBorder(ItemBorder.LEFT, 12);
		super.setItemBorder(ItemBorder.RIGHT, 14);
		super.setItem(barracks, 15);
		super.setItemBorder(ItemBorder.LEFT, 16);
		super.setItemBorder(ItemBorder.TOP, 17);
		super.setItemBorder(ItemBorder.TOP, 20);
		super.setItemBorder(ItemBorder.TOP, 24);
	}

	@Override
	public void onView() {
		// Don't need to do anything yet.
	}
	
	@Override
	public void onClose(boolean openingAnotherView){
		// Don't need to do anything yet.
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		if(currentItem.equals(createAClass)){
			viewController.showView(viewController.getClassesView());
		}else if(currentItem.equals(barracks)){
			viewController.showView(viewController.getBarracksView());
		}else if(currentItem.equals(goToWebsite)){
			viewController.closeCurrentView();
			viewController.getPlayerData().getPlayer().sendMessage(Utils.getInfoString("Website") +  
					ChatColor.GRAY + "Click to go to the website ➡ " + Utils.getWebsiteLink());
		}
	}
}
