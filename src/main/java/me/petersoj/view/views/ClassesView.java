package me.petersoj.view.views;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Rails;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;

public class ClassesView extends View {
	
	private ItemStack lockedClass;
	private WeaponClass currentWeaponClass;
	private Weapon currentWeaponSelection;

	public ClassesView(ViewController viewController) {
		super(viewController, ChatColor.GREEN + "Create A Class", 6, true);
		
		this.lockedClass = new ItemStack(Material.FIRE); // TODO Lock material value
		this.currentWeaponClass = viewController.getPlayerData().getWeaponClasses()[0];
	}

	@Override
	public void createLores() {
		ArrayList<String> lockedClassLore = new ArrayList<String>();
		lockedClassLore.add("");
		lockedClassLore.add(ChatColor.WHITE + "This class is locked, but you");
		lockedClassLore.add(ChatColor.WHITE + "can purchase this class and many");
		lockedClassLore.add(ChatColor.WHITE + "more things at www.block-ops.com/store");
		Utils.setNameAndLore(lockedClass, ChatColor.DARK_RED + "" + ChatColor.BOLD + "Class Locked", lockedClassLore);
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 1);
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 3);
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 5);
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 7);
		super.setItemBorder(ItemBorder.TOP, 9);
		super.setItemBorder(ItemBorder.BOTTOM, 10);
		super.setItemBorder(ItemBorder.TOP_BOTTOM, 11);
		super.setItemBorder(ItemBorder.TOP, 13);
		super.setItemBorder(ItemBorder.BOTTOM, 14);
		super.setItemBorder(ItemBorder.TOP_BOTTOM, 15);
		super.setItemBorder(ItemBorder.TOP, 17);
		super.setItemBorder(ItemBorder.RIGHT, 18);
		super.setItemBorder(ItemBorder.TOP_BOTTOM, 28);
		super.setItemBorder(ItemBorder.TOP, 29);
		super.setItemBorder(ItemBorder.TOP_BOTTOM, 32);
		super.setItemBorder(ItemBorder.TOP_BOTTOM, 33);
		super.setItemBorder(ItemBorder.RIGHT, 36);
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 38);
		super.setItemBorder(ItemBorder.LEFT_RIGHT, 40);
		super.setItemBorder(ItemBorder.LEFT, 44);
		super.setItemBorder(ItemBorder.TOP, 46);
		super.setItemBorder(ItemBorder.TOP, 48);
		super.setItemBorder(ItemBorder.TOP, 40);
		super.setItemBorder(ItemBorder.TOP, 50);
		super.setItemBorder(ItemBorder.TOP, 51);
		super.setItemBorder(ItemBorder.TOP, 52);
	}

	@Override
	public void onView() {
		int pos = 0;
		for(WeaponClass weaponClass : super.getViewController().getPlayerData().getWeaponClasses()){
			if(weaponClass != null){
				boolean glow = false;
				if(weaponClass.equals(currentWeaponClass)){
					glow = true;
				}
				Utils.setItemGlowing(weaponClass.getClassItemStack(), super.getInventory(), pos, glow); // this sets the item as well
			}else{
				super.setItem(lockedClass, pos);
			}
			pos += 2;
		}
		super.setItem(currentWeaponClass.getPrimary().getItemStackBase(), 19);
		super.setItem(currentWeaponClass.getPrimary().getAttachment1().getItemStackBase(), 20);
		super.setItem(currentWeaponClass.getSecondary().getItemStackBase(), 23);
		super.setItem(currentWeaponClass.getSecondary().getAttachment1().getItemStackBase(), 24);
		if(/*currentWeaponClass.getPerk3() instanceof Rails*/false){
			super.setItemBorder(ItemBorder.BOTTOM, 12);
			super.setItemBorder(ItemBorder.BOTTOM, 16);
			super.setItem(currentWeaponClass.getPrimary().getAttachment2().getItemStackBase(), 21);
			super.setItemBorder(ItemBorder.LEFT_RIGHT, 22);
			super.setItem(currentWeaponClass.getSecondary().getAttachment2().getItemStackBase(), 25);
			super.setItemBorder(ItemBorder.LEFT, 26);
			super.setItemBorder(ItemBorder.TOP_BOTTOM, 30);
			super.setItemBorder(ItemBorder.TOP_BOTTOM, 34);
		}else{
			super.clearItem(12);
			super.clearItem(16);
			super.setItemBorder(ItemBorder.LEFT, 21);
			super.setItemBorder(ItemBorder.RIGHT, 22);
			super.setItemBorder(ItemBorder.LEFT, 25);
			super.clearItem(26);
			super.setItemBorder(ItemBorder.BOTTOM, 30);
			super.setItemBorder(ItemBorder.BOTTOM, 34);
		}
		super.setItem(currentWeaponClass.getLethal().getItemStackBase(), 37);
		super.setItem(currentWeaponClass.getTactical().getItemStackBase(), 39);
		super.setItem(currentWeaponClass.getPerk1().getItemStackBase(), 41);
		super.setItem(currentWeaponClass.getPerk2().getItemStackBase(), 42);
		super.setItem(currentWeaponClass.getPerk3().getItemStackBase(), 43);
	}

	@Override
	public void onClose(boolean openingAnotherView) {
		if(!openingAnotherView){
			for(WeaponClass weaponClass : super.getViewController().getPlayerData().getWeaponClasses()){
				if(weaponClass != null){
					Utils.setItemGlowing(weaponClass.getClassItemStack(), null, 0, false);
				}
			}
		}
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		PlayerData playerData = super.getViewController().getPlayerData();
		ViewController viewController = super.getViewController();
		
		if(super.getBackButton().equals(currentItem)){
			viewController.showView(viewController.getMenuView());
			return;
		}
		
		for(WeaponClass weaponClass : playerData.getWeaponClasses()){
			if(weaponClass != null && weaponClass.getClassItemStack().equals(currentItem)){
				this.currentWeaponClass = weaponClass;
				viewController.showView(this);
				return;
			}
		}
		
		if(currentItem.equals(lockedClass)){
			viewController.closeCurrentView();
			playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.RED + "" + ChatColor.BOLD + "You do not have this class!");
			playerData.getPlayer().sendMessage(Utils.getSpaceInfoString("Create A Class") + ChatColor.GRAY + "You can purchase/unlock this class at " + Utils.getWebsiteLink());
			return;
		}
		
		if(currentWeaponClass.getPrimary().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getPrimary();
			viewController.getGunSelectionView().setCurrentTabType(currentWeaponClass.getPrimary().getGunType());
			viewController.showView(viewController.getGunSelectionView());
			
		}else if(currentWeaponClass.getPrimary().getAttachment1().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getPrimary().getAttachment1();
			viewController.showView(viewController.getAttachmentSelectionView());
			
		}else if(/*currentWeaponClass.getPerk3() instanceof Rails*/ false){
			if(currentWeaponClass.getPrimary().getAttachment2().getItemStackBase().equals(currentItem)){
				currentWeaponSelection = currentWeaponClass.getPrimary().getAttachment2();
				viewController.showView(viewController.getAttachmentSelectionView());
				
			}else if(currentWeaponClass.getSecondary().getAttachment2().getItemStackBase().equals(currentItem)){
				currentWeaponSelection = currentWeaponClass.getSecondary().getAttachment2();
				viewController.showView(viewController.getAttachmentSelectionView());
				
			}
		}else if(currentWeaponClass.getSecondary().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getSecondary();
			viewController.getGunSelectionView().setCurrentTabType(currentWeaponClass.getSecondary().getGunType());
			viewController.showView(viewController.getGunSelectionView());
			
		}else if(currentWeaponClass.getSecondary().getAttachment1().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getSecondary().getAttachment1();
			viewController.showView(viewController.getAttachmentSelectionView());
			
		}else if(currentWeaponClass.getLethal().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getLethal();
			viewController.showView(viewController.getGrenadeSelectionView());
			
		}else if(currentWeaponClass.getTactical().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getTactical();
			viewController.showView(viewController.getGrenadeSelectionView());
			
		}else if(currentWeaponClass.getPerk1().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getPerk1();
			viewController.showView(viewController.getPerkSelectionView());

		}else if(currentWeaponClass.getPerk2().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getPerk2();
			viewController.showView(viewController.getPerkSelectionView());
			
		}else if(currentWeaponClass.getPerk3().getItemStackBase().equals(currentItem)){
			currentWeaponSelection = currentWeaponClass.getPerk3();
			viewController.showView(viewController.getPerkSelectionView());
		}
	}
	
	public WeaponClass getCurrentWeaponClass(){
		return currentWeaponClass;
	}

	public Weapon getCurrentWeaponSelection() {
		return currentWeaponSelection;
	}
}
