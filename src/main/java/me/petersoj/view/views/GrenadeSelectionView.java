package me.petersoj.view.views;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.grenade.Grenade;
import me.petersoj.weapon.grenade.GrenadeType;

public class GrenadeSelectionView extends View {

	public GrenadeSelectionView(ViewController viewController) {
		super(viewController, ChatColor.DARK_GRAY + "Select a Grenade", 3, true);
	}

	@Override
	public void createLores() {
		
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.BOTTOM, 1);
		super.setItemBorder(ItemBorder.BOTTOM, 2);
		super.setItemBorder(ItemBorder.RIGHT, 9);
		super.setItemBorder(ItemBorder.LEFT, 12);
		super.setItemBorder(ItemBorder.TOP, 19);
		super.setItemBorder(ItemBorder.TOP, 20);
	}

	@Override
	public void onView() {
		WeaponClass currentWeaponClass = super.getViewController().getClassesView().getCurrentWeaponClass();
		Grenade currentGrenade = (Grenade) super.getViewController().getClassesView().getCurrentWeaponSelection();
		
		int grenadePosistion = 10;
		for(Grenade grenade : currentWeaponClass.getGrenades()){
			if(grenade.getGrenadeType() == currentGrenade.getGrenadeType()){
				if(grenade.equals(currentGrenade)){
					Utils.setItemGlowing(grenade.getItemStackBase(), super.getInventory(), grenadePosistion++, true);
				}else{
					super.setItem(grenade.getItemStackBase(), grenadePosistion++);
				}
			}
		}
	}

	@Override
	public void onClose(boolean openingAnotherView) {
		Utils.setItemGlowing(super.getViewController().getClassesView().getCurrentWeaponSelection().getItemStackBase(), null, 0, false);
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		if(currentItem.equals(super.getBackButton())){
			viewController.showView(viewController.getClassesView());
			return;
		}
		
		PlayerData playerData = viewController.getPlayerData();
		
		for(Grenade grenade : viewController.getClassesView().getCurrentWeaponClass().getGrenades()){
			if(grenade.getItemStackBase().equals(currentItem) &&
					((Grenade) viewController.getClassesView().getCurrentWeaponSelection()).getGrenadeType() == grenade.getGrenadeType()){ // Gets the grenade that they clicked in the view
				WeaponPossession weaponPossession = grenade.getWeaponPossession();
				if(weaponPossession == WeaponPossession.UNBOUGHT){
					if(playerData.getCoins() >= grenade.getPrice() && playerData.getPlayerLevel(false) >= grenade.getUnlockLevel()){
						viewController.getConfirmWeaponPurchaseView().setWeaponToBuy(grenade);
						viewController.showView(viewController.getConfirmWeaponPurchaseView());
					}else{
						playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.RED + "You cannot buy this weapon yet!");
					}
				}else if(weaponPossession == WeaponPossession.BOUGHT){
					if(grenade.getGrenadeType() == GrenadeType.LETHAL){
						viewController.getClassesView().getCurrentWeaponClass().setLethal(grenade);
					}else if(grenade.getGrenadeType() == GrenadeType.TACTICAL){
						viewController.getClassesView().getCurrentWeaponClass().setTactical(grenade);
					}
					grenade.setWeaponPossession(WeaponPossession.SELECTED);
					viewController.showView(viewController.getClassesView());
				}else if(weaponPossession == WeaponPossession.SELECTED){
					viewController.showView(viewController.getClassesView());
				}
				break; // No need to loop after we know the clicked item
			}
		}
		
	}
}
