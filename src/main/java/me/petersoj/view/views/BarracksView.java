package me.petersoj.view.views;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.WeaponClass;

public class BarracksView extends View {
	
	private ItemStack serverStats;
	private ItemStack warStats;
	private ItemStack prestigeMode;

	public BarracksView(ViewController viewController) {
		super(viewController, ChatColor.GOLD + "Barracks", 3, true);
		serverStats = new ItemStack(Material.BOOK);
		warStats = new ItemStack(Material.DIAMOND_SWORD);
		prestigeMode = new ItemStack(Material.BLAZE_POWDER);
	}
	
	@Override
	public void createLores() {
		PlayerData playerData = super.getViewController().getPlayerData();
		
		ArrayList<String> barracksInfoLore = new ArrayList<String>();
		barracksInfoLore.add("");
		barracksInfoLore.add(ChatColor.GREEN + "Rank: " + playerData.getRank().toString());
		barracksInfoLore.add("");
		barracksInfoLore.add(ChatColor.GREEN + "Level: " + ChatColor.WHITE + ChatColor.BOLD + playerData.getPlayerLevel(false));
		barracksInfoLore.add(ChatColor.DARK_GREEN + "  XP: " + ChatColor.GRAY + playerData.getPlayerLevel(false));
		int xpNeeded = playerData.getXpFromLevel(playerData.getPlayerLevel(false) + 1) - playerData.getXp();
		barracksInfoLore.add(ChatColor.DARK_GREEN + "  XP to next level: " + ChatColor.GRAY + xpNeeded);
		barracksInfoLore.add("");
		barracksInfoLore.add(ChatColor.GREEN + "Prestige Level: " + ChatColor.WHITE + ChatColor.BOLD + playerData.getPrestigeLevel());
		barracksInfoLore.add(ChatColor.DARK_GREEN + "  Levels to Prestige: " + ChatColor.GRAY + (50 - playerData.getPlayerLevel(false)));
		barracksInfoLore.add("");
		barracksInfoLore.add(ChatColor.YELLOW + "Coins: " + ChatColor.GOLD + ChatColor.BOLD + playerData.getCoins());
		barracksInfoLore.add("");
		barracksInfoLore.add(ChatColor.BLUE + "Time Played: " + ChatColor.WHITE + this.formatTimePlayed(playerData.getTimePlayed()));
		Utils.setNameAndLore(serverStats, ChatColor.GOLD + "" + ChatColor.BOLD + "Server Stats", barracksInfoLore);
		
		ArrayList<String> warStatsLore = new ArrayList<String>();
		warStatsLore.add("");
		warStatsLore.add(ChatColor.DARK_RED + "Kills: " + ChatColor.GRAY + ChatColor.BOLD + playerData.getKills());
		warStatsLore.add("");
		warStatsLore.add(ChatColor.GRAY + "Deaths: " + ChatColor.GRAY + ChatColor.BOLD + playerData.getDeaths());
		warStatsLore.add("");
		warStatsLore.add(ChatColor.GREEN + "Wins: " + ChatColor.WHITE + ChatColor.BOLD + playerData.getWins());
		warStatsLore.add("");
		warStatsLore.add(ChatColor.RED + "Losses: " + ChatColor.GRAY + ChatColor.BOLD + playerData.getLosses());
		Utils.setNameAndLore(warStats, ChatColor.RED + "" + ChatColor.BOLD + "War Stats", warStatsLore);
		
		ArrayList<String> prestigeModeLore = new ArrayList<String>();
		prestigeModeLore.add("");
		if(playerData.getPlayerLevel(false) < 50){
			prestigeModeLore.add(ChatColor.RED + "" + ChatColor.BOLD + "You must be level 50 to");
			prestigeModeLore.add(ChatColor.RED + "" + ChatColor.BOLD + "unlock prestige mode!");
		}else{
			prestigeModeLore.add(ChatColor.WHITE + "" + ChatColor.BOLD + "Note: ");
			prestigeModeLore.add(ChatColor.WHITE + "When entering into the next");
			prestigeModeLore.add(ChatColor.WHITE + "Prestige mode your: ");
			prestigeModeLore.add(ChatColor.GRAY + "  - Level will return to 1");
			prestigeModeLore.add(ChatColor.GRAY + "  - Coins, XP, Kills, Deaths,");
			prestigeModeLore.add(ChatColor.GRAY + "    Wins, and Losses will be 0");
			prestigeModeLore.add(ChatColor.GRAY + "  - Weapon Classes will be reset");
			prestigeModeLore.add(ChatColor.GRAY + "  - Name will stand out in chat");
			prestigeModeLore.add("");
			prestigeModeLore.add(ChatColor.RED + "NOTE: " + ChatColor.WHITE + "Any store purchased content");
			prestigeModeLore.add(ChatColor.WHITE + "      will not be reset and can");
			prestigeModeLore.add(ChatColor.WHITE + "      be used at level 1 :)");
			prestigeModeLore.add("");
			prestigeModeLore.add(ChatColor.GOLD + "Click to become prestige!");
			prestigeModeLore.add("");
			prestigeModeLore.add(ChatColor.DARK_RED + "" + ChatColor.BOLD + "WARNING: This cannot be undone!");
		}
		Utils.setNameAndLore(prestigeMode, ChatColor.GOLD + "Enter into Prestige Mode!", prestigeModeLore);
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.BOTTOM, 2);
		super.setItemBorder(ItemBorder.BOTTOM, 3);
		super.setItemBorder(ItemBorder.BOTTOM, 6);
		super.setItemBorder(ItemBorder.RIGHT, 10);
		super.setItem(serverStats, 11);
		super.setItem(warStats, 12);
		super.setItemBorder(ItemBorder.LEFT, 13);
		super.setItemBorder(ItemBorder.RIGHT, 14);
		super.setItem(prestigeMode, 15);
		super.setItemBorder(ItemBorder.LEFT, 16);
		super.setItemBorder(ItemBorder.TOP, 20);
		super.setItemBorder(ItemBorder.TOP, 21);
		super.setItemBorder(ItemBorder.TOP, 24);
	}

	@Override
	public void onView() {
		this.createLores();
	}
	
	@Override
	public void onClose(boolean openingAnotherView){
		// Nothing to see here.
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		 if(super.getBackButton().equals(currentItem)){
			 viewController.showView(viewController.getMenuView());
		}
		
		PlayerData playerData = viewController.getPlayerData();
		
		if(currentItem.equals(prestigeMode)){
			if(playerData.getPlayerLevel(false) >= 50){
				playerData.incrementXp(-playerData.getXp());
				playerData.incrementKills(-playerData.getKills());
				playerData.incrementDeaths(-playerData.getDeaths());
				playerData.incrementWins(-playerData.getWins());
				playerData.incrementLosses(-playerData.getLosses());
				for(WeaponClass weaponClass : playerData.getWeaponClasses()){
					if(weaponClass == null){
						continue;
					}
					weaponClass.setIsDefaultClass();
				}
				playerData.incrementPrestigeLevel(1);
				
				playerData.getPlayer().sendMessage(Utils.getInfoString("Level") + ChatColor.GOLD + "Congrats!");
				playerData.getPlayer().sendMessage(Utils.getSpaceInfoString("Level") + ChatColor.WHITE + "You are now Prestige Level " + ChatColor.GOLD + playerData.getPrestigeLevel());
				playerData.getPlayer().playSound(playerData.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
			}else{
				playerData.getPlayer().sendMessage(Utils.getInfoString("Level") + ChatColor.RED + "You are not level 50!");
			}
			viewController.closeCurrentView();
		}
	}
	
	public String formatTimePlayed(int timePlayed){
		if(timePlayed <= 3600){ // minutes
			return String.valueOf(TimeUnit.MINUTES.convert(timePlayed, TimeUnit.SECONDS) + " minutes");
		}else{ // hours
			int timeLeft = timePlayed;
			long hoursPlayed = TimeUnit.HOURS.convert(timeLeft, TimeUnit.SECONDS);
			timeLeft -= hoursPlayed * 3600;
			long minutesPlayedLeftOver = TimeUnit.MINUTES.convert(timeLeft, TimeUnit.SECONDS);
			return String.valueOf(hoursPlayed + " hours and" + minutesPlayedLeftOver + " minutes");
		}
	}
}
