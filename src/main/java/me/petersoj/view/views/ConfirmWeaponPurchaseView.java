package me.petersoj.view.views;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponPossession;

public class ConfirmWeaponPurchaseView extends View {
	
	private ItemStack purchaseWeaponInfo;
	private ItemStack checkMark;
	private ItemStack cancelButton;
	
	private Weapon weaponToBuy;
	
	public ConfirmWeaponPurchaseView(ViewController viewController) {
		super(viewController, ChatColor.GOLD + "Confirm Purchase", 4, true);
		
		this.purchaseWeaponInfo = new ItemStack(Material.BOOK);
		this.checkMark = new ItemStack(Material.FIRE);
		this.cancelButton = new ItemStack(Material.FIRE); // TODO Accurate values ^
	}

	@Override
	public void createLores() {
		ArrayList<String> purchaseWeaponInfoLore = new ArrayList<String>();
		purchaseWeaponInfoLore.add("");
		purchaseWeaponInfoLore.add("");
		Utils.setNameAndLore(purchaseWeaponInfo, ChatColor.BLUE + "" + ChatColor.BOLD + "Weapon Info", purchaseWeaponInfoLore);
		
		Utils.setNameAndLore(checkMark, ChatColor.GREEN + "" + ChatColor.BOLD + "Purchase", null);
		Utils.setNameAndLore(cancelButton, ChatColor.RED + "" + ChatColor.BOLD + "Cancel", null);
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.BOTTOM, 4);
		super.setItemBorder(ItemBorder.RIGHT, 12);
		super.setItem(purchaseWeaponInfo, 13);
		super.setItemBorder(ItemBorder.LEFT, 14);
		super.setItemBorder(ItemBorder.BOTTOM, 18);
		super.setItemBorder(ItemBorder.TOP, 22);
		super.setItemBorder(ItemBorder.BOTTOM, 26);
		super.setItem(checkMark, 27);
		super.setItemBorder(ItemBorder.LEFT, 28);
		super.setItemBorder(ItemBorder.RIGHT, 34);
		super.setItem(cancelButton, 35);
	}

	@Override
	public void onView() {
		this.purchaseWeaponInfo.getItemMeta().getLore().set(1, ChatColor.GOLD + "" +
				super.getViewController().getClassesView().getCurrentWeaponSelection().getPrice() + ChatColor.WHITE + " will be deducted from your balance.");
	}

	@Override
	public void onClose(boolean openingAnotherView) {
		
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		if(currentItem.equals(super.getBackButton())){
			super.getViewController().showView(super.getViewController().getClassesView());
			return;
		}
		
		ViewController viewController = super.getViewController();
		PlayerData playerData = viewController.getPlayerData();
		
		if(currentItem.equals(checkMark)){
			if(weaponToBuy.getWeaponPossession() == WeaponPossession.UNBOUGHT && playerData.getCoins() >= weaponToBuy.getPrice()){
				playerData.incrementCoins(-weaponToBuy.getPrice()); // Take away the weapons price coins from their account balance
				weaponToBuy.setWeaponPossession(WeaponPossession.BOUGHT);
				viewController.showView(viewController.getClassesView());
				playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.WHITE + "You have successfully bought the "
						+ ChatColor.GRAY + weaponToBuy.getName());
				playerData.getPlayer().sendMessage(Utils.getSpaceInfoString("Create A Class") + ChatColor.GRAY + "You now have: "
						+ ChatColor.GOLD + playerData.getCoins() + ChatColor.GRAY + " coins.");
				playerData.getPlayer().sendMessage(Utils.getSpaceInfoString("Create A Class") + ChatColor.GRAY + "Now go and select the weapon that you have just bought!");
			}
		}else if(currentItem.equals(cancelButton)){
			viewController.showView(viewController.getClassesView());
		}
	}
	
	public void setWeaponToBuy(Weapon weaponToBuy){
		this.weaponToBuy = weaponToBuy; 
	}

}
