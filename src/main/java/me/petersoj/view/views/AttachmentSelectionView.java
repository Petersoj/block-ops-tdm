package me.petersoj.view.views;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.attachment.Attachment;

public class AttachmentSelectionView extends View {
	
	public AttachmentSelectionView(ViewController viewController) {
		super(viewController, "Select an Attachment", 3, true);
	}
	
	@Override
	public void createLores() {
		
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.RIGHT, 9);
	}

	@Override
	public void onView() {
		Attachment currentAttachment = (Attachment) super.getViewController().getClassesView().getCurrentWeaponSelection();
		ArrayList<Attachment> currentGunAttachments = currentAttachment.getAttachedGun().getAttachments();
		
		/*
		for(int pos = 1; pos < currentGunAttachments.size() + 1; pos++){
			super.setItemBorder(ItemBorder.BOTTOM, pos);
		}
		 */
		
		int attachmentPosition = 10;
		for(Attachment attachment : currentGunAttachments){
			super.setItemBorder(ItemBorder.BOTTOM, attachmentPosition - 9);
			super.setItemBorder(ItemBorder.TOP, attachmentPosition + 9);
			if(attachment.equals(currentAttachment)){
				Utils.setItemGlowing(currentAttachment.getItemStackBase(), super.getInventory(), attachmentPosition++, true);
			}else{
				super.setItem(attachment.getItemStackBase(), attachmentPosition++);
			}
		}
		
		super.setItemBorder(ItemBorder.LEFT, currentGunAttachments.size() + 10);
		
		/*
		for(int pos = 19; pos < currentGunAttachments.size() + 19; pos++){
			super.setItemBorder(ItemBorder.TOP, pos);
		}
		 */
	}
	
	@Override
	public void onClose(boolean openingAnotherView) {
		Utils.setItemGlowing(super.getViewController().getClassesView().getCurrentWeaponSelection().getItemStackBase(), null, 0, false);
	}
	
	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		if(currentItem.equals(super.getBackButton())){
			viewController.showView(viewController.getClassesView());
			return;
		}
		
		PlayerData playerData  = viewController.getPlayerData();
		
		// Looping because all the attachments are being showing of the attachedGun
		for(Attachment attachment : ((Attachment) viewController.getClassesView().getCurrentWeaponSelection()).getAttachedGun().getAttachments()){
			if(attachment.getItemStackBase().equals(currentItem)){ // Gets the attachment that they clicked
				WeaponPossession weaponPossession = attachment.getWeaponPossession();
				if(weaponPossession == WeaponPossession.UNBOUGHT){
					if(playerData.getCoins() >= attachment.getPrice() && attachment.getAttachedGun().getGunLevel() >= attachment.getUnlockLevel()){
						viewController.getConfirmWeaponPurchaseView().setWeaponToBuy(attachment);
						viewController.showView(viewController.getConfirmWeaponPurchaseView());
					}else{
						playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.RED + "You cannot buy this weapon!");
					}
				}else if(weaponPossession == WeaponPossession.BOUGHT){
					if(attachment.equals(attachment.getAttachedGun().getAttachment2())){ // We don't have to check for null because attachment won't == null 
						attachment.getAttachedGun().setAttachment2(attachment);
					}else{
						attachment.getAttachedGun().setAttachment1(attachment);
					}
					attachment.setWeaponPossession(WeaponPossession.SELECTED);
					viewController.showView(viewController.getClassesView());
				}else if(weaponPossession == WeaponPossession.SELECTED){ // Two attachments selected if they have perk 3 RAILS
					if(attachment.equals(viewController.getClassesView().getCurrentWeaponSelection())){
						viewController.showView(viewController.getClassesView());
					}else{ // We only want one attachment of one kind to be in either of the Attachment slots
						playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.GRAY + "You already have this attachment selected!");
					}
				}
				break; // No need to loop after we know the clicked item
			}
		}
	}
}