package me.petersoj.view.views;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.perk.Perk;
import me.petersoj.weapon.perk.PerkType;

public class PerkSelectionView extends View {

	public PerkSelectionView(ViewController viewController) {
		super(viewController, ChatColor.RED + "Select A Perk", 3, true);
	}

	@Override
	public void createLores() {
		
	}

	@Override
	public void setupItemPositionConstants() {
		super.setItemBorder(ItemBorder.RIGHT, 9);
	}

	@Override
	public void onView() {
		Perk currentPerk = (Perk) super.getViewController().getClassesView().getCurrentWeaponSelection();
		
		super.setItemBorder(ItemBorder.RIGHT, 9);
		int perkPosition = 10;
		for(Perk perk : super.getViewController().getClassesView().getCurrentWeaponClass().getPerks()){
			if(perk.getPerkType() == currentPerk.getPerkType()){
				ItemStack bottomBorder = super.getInventory().getItem(perkPosition - 9);
				if(bottomBorder != null && bottomBorder.getType() != Material.AIR){ // border placement
					super.setItemBorder(ItemBorder.TOP_BOTTOM, perkPosition - 9);
				}else{
					super.setItemBorder(ItemBorder.BOTTOM, perkPosition - 9);
				}
				super.setItemBorder(ItemBorder.TOP, perkPosition + 9);
				
				boolean glow = false;
				if(perk.equals(currentPerk)){
					glow = true;
				}
				Utils.setItemGlowing(perk.getItemStackBase(), super.getInventory(), perkPosition++, glow);
			}
		}
		super.setItemBorder(ItemBorder.LEFT, perkPosition);
	}

	@Override
	public void onClose(boolean openingAnotherView) {
		Utils.setItemGlowing(super.getViewController().getClassesView().getCurrentWeaponSelection().getItemStackBase(), null, 0, false);
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		if(currentItem.equals(super.getBackButton())){
			viewController.showView(viewController.getClassesView());
			return;
		}
		
		WeaponClass weaponClass = viewController.getClassesView().getCurrentWeaponClass();
		PlayerData playerData = viewController.getPlayerData();
		
		for(Perk perk : weaponClass.getPerks()){
			if(perk.getItemStackBase().equals(currentItem)){ // the perk object that was clicked
				WeaponPossession weaponPossession = perk.getWeaponPossession();
				if(weaponPossession == WeaponPossession.UNBOUGHT){
					if(playerData.getCoins() >= perk.getPrice() && playerData.getPlayerLevel(false) >= perk.getUnlockLevel()){
						viewController.getConfirmWeaponPurchaseView().setWeaponToBuy(perk);
						viewController.showView(viewController.getConfirmWeaponPurchaseView());
					}else{
						playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.RED + "You cannot buy this weapon yet!");
					}
				}else if(weaponPossession == WeaponPossession.BOUGHT){
					if(perk.getPerkType() == PerkType.TIER_1){
						weaponClass.setPerk1(perk);
					}else if(perk.getPerkType() == PerkType.TIER_2){
						weaponClass.setPerk2(perk);
					}else if(perk.getPerkType() == PerkType.TIER_3){
						weaponClass.setPerk3(perk);
					}
					perk.setWeaponPossession(WeaponPossession.SELECTED);
					viewController.showView(viewController.getClassesView());
				}else if(weaponPossession == WeaponPossession.SELECTED){
					viewController.showView(viewController.getClassesView());
				}
				break; // No need to loop after we know the clicked item
			}
		}
		
	}

}
