package me.petersoj.view.views;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.view.ItemBorder;
import me.petersoj.view.View;
import me.petersoj.view.ViewController;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.gun.Gun;
import me.petersoj.weapon.gun.GunType;

public class GunSelectionView extends View {
	
	private ItemStack smgTypeItem;
	private ItemStack assaultRifleTypeItem;
	private ItemStack shotgunTypeItem;
	private ItemStack sniperTypeItem;
	
	private ItemStack pistolTypeItem;
	private ItemStack specialTypeItem;
	
	private GunType currentTabType;

	public GunSelectionView(ViewController viewController) {
		super(viewController, ChatColor.RED + "Select a Gun", 4, true);
		
		// TODO accurate values
		this.smgTypeItem = new ItemStack(Material.FIRE); // UMP .45
		this.assaultRifleTypeItem = new ItemStack(Material.FIRE); // ACR
		this.shotgunTypeItem = new ItemStack(Material.FIRE); // SPAS 12
		this.sniperTypeItem = new ItemStack(Material.FIRE); // Barret .50 Cal
		
		this.pistolTypeItem = new ItemStack(Material.FIRE); // USP .45
		this.specialTypeItem = new ItemStack(Material.FIRE); // Locked symbol (Coming soon)
	}

	@Override
	public void createLores() {
		Utils.setNameAndLore(smgTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "SMGs", null);
		Utils.setNameAndLore(assaultRifleTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "Assault Rifles", null);
		Utils.setNameAndLore(shotgunTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "Shotguns", null);
		Utils.setNameAndLore(sniperTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "Snipers", null);
		
		Utils.setNameAndLore(pistolTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "Pistols", null);
		Utils.setNameAndLore(specialTypeItem, ChatColor.BLUE + "" + ChatColor.BOLD + "Specials (coming soon)", null);
	}

	@Override
	public void setupItemPositionConstants() {
		// This is a very dynamic view
	}

	@Override
	public void onView() {
		ViewController viewController = super.getViewController();
		Gun currentGun = (Gun) viewController.getClassesView().getCurrentWeaponSelection();
		WeaponClass currentWeaponClass = viewController.getClassesView().getCurrentWeaponClass();
		
		super.getInventory().clear(); // Clear everything out of the inventory every view.
		
		Utils.setItemGlowing(this.getTabTypeItemStackFromType(), null, 0, true);
		if(currentGun.equals(currentWeaponClass.getPrimary())){ // They clicked a primary
			super.setItemBorder(ItemBorder.RIGHT, 0);
			super.setItem(smgTypeItem, 1);
			super.setItemBorder(ItemBorder.LEFT_RIGHT, 2);
			super.setItem(assaultRifleTypeItem, 3);
			super.setItemBorder(ItemBorder.LEFT_RIGHT, 4);
			super.setItem(shotgunTypeItem, 5);
			super.setItemBorder(ItemBorder.LEFT_RIGHT, 6);
			super.setItem(sniperTypeItem, 7);
			super.setItemBorder(ItemBorder.LEFT, 8);
		}else if(currentGun.equals(currentWeaponClass.getSecondary())){ // They clicked a secondary
			super.setItemBorder(ItemBorder.RIGHT, 1);
			super.setItem(pistolTypeItem, 2);
			super.setItemBorder(ItemBorder.LEFT, 3);
			super.setItemBorder(ItemBorder.RIGHT, 5);
			super.setItem(specialTypeItem, 6);
			super.setItemBorder(ItemBorder.LEFT, 7);
		}
		super.setItemBorder(ItemBorder.RIGHT, 9);
		
		Inventory inventory = super.getInventory();
		int gunPosition = 10;
		for(Gun gun : currentWeaponClass.getGuns()){
			if(gun.getGunType() == currentTabType){
				ItemStack bottomBorder = inventory.getItem(gunPosition - 9);
				if(bottomBorder != null && bottomBorder.getType() != Material.AIR){ // border placement
					super.setItemBorder(ItemBorder.TOP_BOTTOM, gunPosition - 9);
				}else{
					super.setItemBorder(ItemBorder.BOTTOM, gunPosition - 9);
				}
				super.setItemBorder(ItemBorder.TOP, gunPosition + 9);
				
				boolean glow = false;
				if(gun.equals(currentGun)){
					glow = true;
				}
				Utils.setItemGlowing(gun.getItemStackBase(), inventory, gunPosition++, glow);
			}
		}
		super.setItemBorder(ItemBorder.LEFT, gunPosition);
	}

	@Override
	public void onClose(boolean openingAnotherView) {
		Utils.setItemGlowing(this.getTabTypeItemStackFromType(), null, 0, false);
		Utils.setItemGlowing(super.getViewController().getClassesView().getCurrentWeaponSelection().getItemStackBase(), null, 0, false);
	}

	@Override
	public void onItemClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if(!super.isValidClickedItemStack(currentItem)){
			return;
		}
		
		ViewController viewController = super.getViewController();
		
		if(currentItem.equals(super.getBackButton())){
			viewController.showView(viewController.getClassesView());
			return;
		}
		
		boolean tabTypeClicked = false;
		
		if(currentItem.equals(smgTypeItem)){
			this.currentTabType = GunType.SMG;
			tabTypeClicked = true;
		}else if(currentItem.equals(assaultRifleTypeItem)){
			this.currentTabType = GunType.ASSAULT_RIFLE;
			tabTypeClicked = true;
		}else if(currentItem.equals(shotgunTypeItem)){
			this.currentTabType = GunType.SHOTGUN;
			tabTypeClicked = true;
		}else if(currentItem.equals(sniperTypeItem)){
			this.currentTabType = GunType.SNIPER_RIFLE;
			tabTypeClicked = true;
		}else if(currentItem.equals(pistolTypeItem)){
			this.currentTabType = GunType.PISTOL;
			tabTypeClicked = true;
		}else if(currentItem.equals(specialTypeItem)){
			this.currentTabType = GunType.SPECIAL;
			tabTypeClicked = true;
		}
		
		if(tabTypeClicked){
			viewController.showView(this);
			return;
		}
		
		WeaponClass weaponClass = viewController.getClassesView().getCurrentWeaponClass();
		PlayerData playerData = viewController.getPlayerData();
		
		for(Gun gun : weaponClass.getGuns()){
			if(gun.getItemStackBase().equals(currentItem)){ // the gun object that was clicked
				WeaponPossession weaponPossession = gun.getWeaponPossession();
				if(weaponPossession == WeaponPossession.UNBOUGHT){
					if(playerData.getCoins() >= gun.getPrice() && playerData.getPlayerLevel(false) >= gun.getUnlockLevel()){
						viewController.getConfirmWeaponPurchaseView().setWeaponToBuy(gun);
						viewController.showView(viewController.getConfirmWeaponPurchaseView());
					}else{
						playerData.getPlayer().sendMessage(Utils.getInfoString("Create A Class") + ChatColor.RED + "You cannot buy this weapon yet!");
					}
				}else if(weaponPossession == WeaponPossession.BOUGHT){
					if(gun.getGunType() == GunType.PISTOL || gun.getGunType() == GunType.SPECIAL){ // secondary
						weaponClass.setSecondary(gun);
					}else{ // primary
						weaponClass.setPrimary(gun);
					}
					gun.setWeaponPossession(WeaponPossession.SELECTED);
					viewController.showView(viewController.getClassesView());
				}else if(weaponPossession == WeaponPossession.SELECTED){
					viewController.showView(viewController.getClassesView());
				}
				break; // No need to loop after we know the clicked item
			}
		}
	}

	
	public ItemStack getTabTypeItemStackFromType(){
		switch(currentTabType){
			case SMG:
				return smgTypeItem;
			case ASSAULT_RIFLE:
				return assaultRifleTypeItem;
			case SHOTGUN:
				return shotgunTypeItem;
			case SNIPER_RIFLE:
				return sniperTypeItem;
			case PISTOL:
				return pistolTypeItem;
			case SPECIAL:
				return specialTypeItem;
			default:
				return smgTypeItem;
		}
	}
	
	public void setCurrentTabType(GunType gunType){
		this.currentTabType = gunType;
	}
}
