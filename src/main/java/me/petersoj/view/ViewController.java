package me.petersoj.view;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.BlockOpsTDM;
import me.petersoj.data.PlayerData;
import me.petersoj.view.views.AttachmentSelectionView;
import me.petersoj.view.views.BarracksView;
import me.petersoj.view.views.ClassesView;
import me.petersoj.view.views.ConfirmWeaponPurchaseView;
import me.petersoj.view.views.GrenadeSelectionView;
import me.petersoj.view.views.GunSelectionView;
import me.petersoj.view.views.MenuView;
import me.petersoj.view.views.PerkSelectionView;

public class ViewController {
	
	private PlayerData playerData;
	private View currentView;
	
	private boolean inventoryOpen;
	
	private AttachmentSelectionView attachmentSelectionView;
	private BarracksView barracksView;
	private ClassesView classesView;
	private ConfirmWeaponPurchaseView confirmWeaponPurchaseView;
	private GrenadeSelectionView grenadeSelectionView;
	private GunSelectionView gunSelectionView;
	private MenuView menuView;
	private PerkSelectionView perkSelectionView;
	
	public ViewController(PlayerData playerData){
		this.playerData = playerData;
		this.inventoryOpen = false;

		this.attachmentSelectionView = new AttachmentSelectionView(this);
		this.barracksView = new BarracksView(this);
		this.classesView = new ClassesView(this);
		this.confirmWeaponPurchaseView = new ConfirmWeaponPurchaseView(this);
		this.grenadeSelectionView = new GrenadeSelectionView(this);
		this.gunSelectionView = new GunSelectionView(this);
		this.menuView = new MenuView(this);
		this.perkSelectionView = new PerkSelectionView(this);
	}
	
	public void onClick(InventoryClickEvent e){
		e.setCancelled(true);
		this.currentView.onItemClick(e);
	}
	
	public void onInventoryClose(InventoryCloseEvent e){
		this.inventoryOpen = false;
		new BukkitRunnable() {
			@Override
			public void run() {
				if(!inventoryOpen){
					currentView.onClose(true);
					currentView = null;
					playerData.getBlockOpsPlayer().giveSpawnItems(); 
				}
			}
		}.runTaskLater(BlockOpsTDM.getInstance(), 2);
	}
	
	public void showView(View view){
		if(currentView != null){
			this.currentView.onClose(false); // old view
			if(view.hasBackButton() && !this.currentView.hasBackButton()){
				this.playerData.getPlayer().getInventory().setItem(9, view.getBackButton());
			}else if(!view.hasBackButton() && this.currentView.hasBackButton()){
				this.playerData.getPlayer().getInventory().clear();
			}
		}else{
			playerData.getPlayer().getInventory().clear();
		}
		view.onView(); // sets the inventory up to bee seen.
		this.currentView = view;
		if(currentView.equals(view)){
			playerData.getPlayer().updateInventory();
		}else{
			playerData.getPlayer().openInventory(view.getInventory());
		}
		this.inventoryOpen = true;
	}
	
	public void closeCurrentView(){
		playerData.getPlayer().closeInventory();
		playerData.getBlockOpsPlayer().giveSpawnItems();
	}
	
	public PlayerData getPlayerData() {
		return playerData;
	}

	public View getCurrentView() {
		return currentView;
	}
	
	public boolean isInventoryOpen(){
		return inventoryOpen;
	}
	
	public AttachmentSelectionView getAttachmentSelectionView() {
		return attachmentSelectionView;
	}
	
	public BarracksView getBarracksView(){
		return barracksView;
	}

	public ClassesView getClassesView() {
		return classesView;
	}

	public ConfirmWeaponPurchaseView getConfirmWeaponPurchaseView() {
		return confirmWeaponPurchaseView;
	}

	public GrenadeSelectionView getGrenadeSelectionView() {
		return grenadeSelectionView;
	}

	public GunSelectionView getGunSelectionView() {
		return gunSelectionView;
	}

	public MenuView getMenuView() {
		return menuView;
	}

	public PerkSelectionView getPerkSelectionView() {
		return perkSelectionView;
	}

}
