package me.petersoj.view;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.petersoj.util.Utils;

public abstract class View {
	
	private ViewController viewController;
	private Inventory inventory;
	
	private ItemStack backButton; 
	
	public View(ViewController viewController, String title, int rows, boolean backButton){
		this.viewController = viewController;
		this.inventory = Bukkit.createInventory(null, rows*9, title);
		
		if(backButton){
			this.backButton = new ItemStack(Material.FIRE); // TODO Fix value
			Utils.setNameAndLore(this.backButton, ChatColor.RED + "Go Back", null);
		}
		
		this.createLores();
		this.setupItemPositionConstants();
	}
	
	public abstract void createLores();
	
	public abstract void setupItemPositionConstants();
	
	public abstract void onView();
	
	public abstract void onClose(boolean openingAnotherView);
	
	public abstract void onItemClick(InventoryClickEvent e);
	
	public ViewController getViewController(){
		return this.viewController;
	}
	
	public void setItem(ItemStack itemStack, int index){
		this.inventory.setItem(index, itemStack);
	}
	
	public void clearItem(int index){
		this.inventory.clear(index);
	}
	
	public void setItemBorder(ItemBorder itemBorder, int posistion){
		this.inventory.setItem(posistion, itemBorder.getItemStack());
	}
	
	public boolean isValidClickedItemStack(ItemStack itemStack){
		if(itemStack == null){
			return false;
		}else if(itemStack.getType() == Material.AIR){
			return false;
		}else{
			for(ItemBorder itemBorder : ItemBorder.values()){
				if(itemBorder.getItemStack().equals(itemStack)){
					return false;
				}
			}
			return true;
		}
	}
	
	public ItemStack getBackButton(){
		return backButton;
	}
	
	public boolean hasBackButton(){
		return backButton != null;
	}
	
	public Inventory getInventory(){
		return inventory;
	}
}