package me.petersoj.world;

import java.util.ArrayList;

import org.bukkit.Location;

public class BlockOpsMap {
	
	private ArrayList<BulletMark> bulletMarks;
	
	public BlockOpsMap(){
		bulletMarks = new ArrayList<BulletMark>();
	}
	
	public void reset(){
		BulletMark.resetAll();
		this.bulletMarks.clear();
	}
	
	public void showBulletMark(Location loc){
		boolean exists = false;
		if(loc.getBlock().getType().isTransparent()){
			return;
		}
		for(BulletMark bulletMark : bulletMarks){
			if(bulletMark.getLocation().equals(loc)){
				bulletMark.showBulletMark();
				exists = true;
			}
		}
		if(!exists){
			BulletMark bulletMark = new BulletMark(this, loc);
			bulletMark.showBulletMark();
			bulletMarks.add(bulletMark);
		}
	}
	
	protected void destroyBulletMark(BulletMark bulletMark){
		bulletMarks.remove(bulletMark);
		/*
		 * Iterator<BulletMark> bulletMarkIterator = bulletMarks.iterator();
		while(bulletMarkIterator.hasNext()){
			if(bulletMarkIterator.next().equals(bulletMark)){
				bulletMarkIterator.remove();
				break;
			}
		}
		 */
	}
}
