package me.petersoj.world;

import me.petersoj.BlockOpsTDM;
import net.minecraft.server.v1_11_R1.BlockPosition;
import net.minecraft.server.v1_11_R1.PacketPlayOutBlockBreakAnimation;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BulletMark {
	
	private static int uniqueID = 0;
	private BlockOpsMap map;
	private Location location;
	private int id;
	private int stage;
	
	public BulletMark(BlockOpsMap map, Location location){
		this.map = map;		
		this.location = location;
		id = uniqueID;
		stage = -1;
		uniqueID++;
	}
	
	public static void resetAll(){
		uniqueID = 0;
	}
	
	public void showBulletMark(){
		if(stage < 9){
			stage++;
		}
		this.sendBlockAnimationPacket();
		
		BulletMark mark = this;
		new BukkitRunnable() {
			@Override
			public void run() {
				stage--;
				if(stage <= -2){
					map.destroyBulletMark(mark);
				}else if(stage <= 9 && stage >= -1){
					sendBlockAnimationPacket();
				}
			}
		}.runTaskLater(BlockOpsTDM.getInstance(), 400); // 20 seconds later
	}
	
	public void sendBlockAnimationPacket(){
		PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(id, new BlockPosition(location.getX(), location.getY(), location.getZ()), stage);
		for(Player p : Bukkit.getOnlinePlayers()){
			((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
		}
	}
	
	public Location getLocation(){
		return this.location;
	}

}
