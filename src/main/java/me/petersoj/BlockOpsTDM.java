package me.petersoj;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.craftbukkit.v1_11_R1.CraftServer;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.mojang.authlib.GameProfile;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.petersoj.game.TDMController;
import me.petersoj.util.Utils;
import net.minecraft.server.v1_11_R1.BlockPosition;
import net.minecraft.server.v1_11_R1.DataWatcher;
import net.minecraft.server.v1_11_R1.DataWatcherObject;
import net.minecraft.server.v1_11_R1.DataWatcherRegistry;
import net.minecraft.server.v1_11_R1.EntityPlayer;
import net.minecraft.server.v1_11_R1.EnumItemSlot;
import net.minecraft.server.v1_11_R1.MinecraftServer;
import net.minecraft.server.v1_11_R1.PacketPlayInTeleportAccept;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_11_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_11_R1.PacketPlayOutOpenSignEditor;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_11_R1.PlayerConnection;
import net.minecraft.server.v1_11_R1.PlayerInteractManager;
import net.minecraft.server.v1_11_R1.WorldServer;

public class BlockOpsTDM extends JavaPlugin {
	
	private static BlockOpsTDM instance;
	private TDMController tdmController;
	
	@Override
	public void onEnable(){
		instance = this;
		tdmController = new TDMController();
		tdmController.setLobbyController();
	}
	
	
	@Override
	public void onDisable(){
		// kick all players still online
		// close redis connection
		
		/* ADD LATER
		 * for(World world : Bukkit.getWorlds()){
			File playerFiles = new File(world.getName() + "/playerdata");
			if(playerFiles.isDirectory()){
				for(File file : playerFiles.listFiles()){
					file.delete();
				}
			}
			File statsFiles = new File(world.getName() + "/stats");
			if(statsFiles.isDirectory()){
				for(File file : statsFiles.listFiles()){
					file.delete();
				}
			}
		}
	 */
	}
	
	public void shutdownServer(int delay){
		// close server within delay
		// kick all players still online
		// close redis connection
		Bukkit.shutdown();
	}
	
	public static BlockOpsTDM getInstance(){
		return instance;
	}
	
	
	
	
	
	
	
	//
	// RANDOM STUFF BELOW
	//
	
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		ChannelDuplexHandler channel = new ChannelDuplexHandler(){
			@Override
			public void channelRead(ChannelHandlerContext context, Object obj) throws Exception{
				super.channelRead(context, obj);
				if(obj instanceof PacketPlayInTeleportAccept){
					Bukkit.broadcastMessage(String.valueOf(((PacketPlayInTeleportAccept) obj).a()));
				}
			}
			
			@Override
			public void write(ChannelHandlerContext context, Object obj, ChannelPromise promise) throws Exception{
				/*
				 * if(obj instanceof PacketPlayOutEntityMetadata){
					PacketPlayOutEntityMetadata pac = (PacketPlayOutEntityMetadata) obj;
					Field field = pac.getClass().getDeclaredField("TADO LATER");
					field.setAccessible(true);
				}
				 */
				super.write(context, obj, promise);
			}
		};
		((CraftPlayer)e.getPlayer()).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", e.getPlayer().getName(), channel);
	}
	
	@EventHandler
	public void onCommandd(PlayerCommandPreprocessEvent e){
		if(e.getMessage().equalsIgnoreCase("/sign")){
			new BukkitRunnable() {
				
				@Override
				public void run() {
					PacketPlayOutOpenSignEditor sign = new PacketPlayOutOpenSignEditor(new BlockPosition(0, 0, 0));
					((CraftPlayer)e.getPlayer()).getHandle().playerConnection.sendPacket(sign);
					
				}
			}.runTaskLater(this, 30);
		}
		if(e.getMessage().equalsIgnoreCase("/item")){
			ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
			PacketPlayOutEntityEquipment equip = new PacketPlayOutEntityEquipment(e.getPlayer().getEntityId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(item));
			((CraftPlayer)e.getPlayer()).getHandle().playerConnection.sendPacket(equip);
		}
		if(e.getMessage().equalsIgnoreCase("/aim")){
			DataWatcher da = new DataWatcher(((CraftPlayer)e.getPlayer()).getHandle());
			da.register(new DataWatcherObject<>(6, DataWatcherRegistry.a), (byte)1);
			for(Player p : Bukkit.getOnlinePlayers()){
				if(p != e.getPlayer()){
					PacketPlayOutEntityMetadata pa = new PacketPlayOutEntityMetadata(e.getPlayer().getEntityId(), da, true);
					((CraftPlayer)p).getHandle().playerConnection.sendPacket(pa);	
				}
			}
		}
		if(e.getMessage().equalsIgnoreCase("/soun")){
			e.getPlayer().playSound(e.getPlayer().getLocation(), "gun.m4a1", 1, 1);
		}
		if(e.getMessage().equalsIgnoreCase("/gta")){
			Player observer = e.getPlayer();
            Location observerPos = observer.getEyeLocation();
            Vector observerDir = observerPos.getDirection();

            Vector observerStart = observerPos.toVector();
            Vector observerEnd = observerStart.clone().add(observerDir.multiply(8));

            Location targetPos = Bukkit.getPlayer("Petersoj").getLocation();
            Vector minimum = targetPos.clone().add(-0.5, 0, -0.5).toVector(); 
            Vector maximum = targetPos.clone().add(0.5, 1.67, 0.5).toVector(); 
			
			observer.getWorld().spigot().playEffect(minimum.toLocation(observerPos.getWorld()), Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			observer.getWorld().spigot().playEffect(maximum.toLocation(observerPos.getWorld()), Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			
			boolean bool = Utils.hasIntersection(observerStart, observerEnd, minimum, maximum);
			
			Bukkit.broadcastMessage(String.valueOf(bool) + " poop");
		}
		if(e.getMessage().equalsIgnoreCase("/nam")){
			DataWatcher watcher = new DataWatcher(((CraftPlayer)e.getPlayer()).getHandle());
			watcher.register(new DataWatcherObject<>(2, DataWatcherRegistry.d), ChatColor.GREEN + "POOP");
			PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(e.getPlayer().getEntityId(), watcher, true);
			for(Player player : Bukkit.getOnlinePlayers()){
				((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
			}
		}
		if(e.getMessage().equalsIgnoreCase("/look")){
			Location eye = e.getPlayer().getEyeLocation();
			Location mEye = Bukkit.getPlayer("Mateothebeast97").getEyeLocation();
			
			Vector vec = mEye.toVector().subtract(eye.toVector()).normalize();
			double dot = vec.dot(eye.getDirection().normalize());
			Bukkit.broadcastMessage(String.valueOf(dot));
		}
		if(e.getMessage().equalsIgnoreCase("/taco")){
			DataWatcher watcher = new DataWatcher(((CraftPlayer)e.getPlayer()).getHandle());
			watcher.register(new DataWatcherObject<>(6, DataWatcherRegistry.a), (byte) 1);
			for(Player p : Bukkit.getOnlinePlayers()){
				((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(((CraftPlayer)e.getPlayer()).getHandle().getId(), watcher, true));
			}
		}
		if(e.getMessage().equalsIgnoreCase("/snowball")){
			new BukkitRunnable() {
				@Override
				public void run() {
					Bukkit.broadcastMessage("snow");
					Location loc = e.getPlayer().getEyeLocation().getDirection().multiply(2).toLocation(e.getPlayer().getWorld()).add(e.getPlayer().getEyeLocation());
					//EntitySnowball snow = new EntitySnowball(((CraftWorld) Bukkit.getWorlds().get(0)).getHandle());
					//snow.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
					//PacketPlayOutSpawnEntity en = new PacketPlayOutSpawnEntity(snow, 11);
					Snowball snow = (Snowball) e.getPlayer().getWorld().spawnEntity(loc, EntityType.SNOWBALL);
					for(Player p : Bukkit.getOnlinePlayers()){
						PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
						p.spawnParticle(Particle.END_ROD, loc.add(0, 0.5, 0), 1, 0, 0, 0, 0);
						//connection.sendPacket(en);
						connection.sendPacket(new PacketPlayOutEntityDestroy(snow.getEntityId()));
					}
				}
			}.runTaskTimer(this, 0, 1);
		}
		if(e.getMessage().equalsIgnoreCase("/up")){
			Inventory inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Gun Classes");
			ItemStack it = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta im = it.getItemMeta();
			im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
			im.setDisplayName(ChatColor.WHITE + "HEY");
			it.setItemMeta(im);
			it.addEnchantment(Enchantment.DIG_SPEED, 1);
			inv.addItem(it);
			e.getPlayer().openInventory(inv);
			it.removeEnchantment(Enchantment.DIG_SPEED);
			e.getPlayer().getInventory().addItem(it);
		}
		if(e.getMessage().startsWith("/speed")){
			e.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(Double.valueOf(e.getMessage().split(" ")[1]));
		}
		if(e.getMessage().startsWith("/walk")){
			e.getPlayer().setWalkSpeed(Float.valueOf(e.getMessage().split(" ")[1]));
		}
		if(e.getMessage().equalsIgnoreCase("/bubble")){
			new BukkitRunnable() {
				@Override
				public void run() {
					Location loc = e.getPlayer().getEyeLocation().getDirection().multiply(2).toLocation(e.getPlayer().getWorld());
					e.getPlayer().spawnParticle(Particle.WATER_BUBBLE, loc.add(e.getPlayer().getEyeLocation()), 1, 0, 0, 0, 1);
				}
			}.runTaskTimer(this, 0, 3);
		}
		if(e.getMessage().equalsIgnoreCase("/ta")){
			MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
			WorldServer nmsWorld = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
			GameProfile profile = new GameProfile(UUID.randomUUID(), ChatColor.GREEN + "Dead body :P");
			profile.getProperties().putAll("textures", ((CraftPlayer)e.getPlayer()).getProfile().getProperties().get("textures"));
			EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, profile, new PlayerInteractManager(nmsWorld));
			Location loc = e.getPlayer().getLocation();
			npc.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
			DataWatcher watcher = new DataWatcher(npc);
			watcher.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), (byte) 0x80);
			for(Player p : Bukkit.getOnlinePlayers()){
				PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
				connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
				connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
				((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(npc.getId(), watcher, true));
				new BukkitRunnable() {
					@Override
					public void run() {
						connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc));
					}
				}.runTaskLater(this, 50L);
			}
		}
		
		if(e.getMessage().equalsIgnoreCase("/ab")){
			Player p = e.getPlayer();
			Player other = Bukkit.getPlayer("dawici");
			
			Location min = other.getLocation().add(-0.3, 0, -0.3);
			Location max = other.getLocation().add(0.3, 1.62, 0.3);
			
			p.getWorld().spigot().playEffect(min, Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			p.getWorld().spigot().playEffect(max, Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			
			Bukkit.broadcastMessage("Min: " + min.toVector().toString() + ", Max: " + max.toVector().toString());
			Bukkit.broadcastMessage(p.getLocation().getDirection().add(p.getEyeLocation().toVector()).toString());
			
			boolean axis = p.getLocation().getDirection().add(p.getEyeLocation().toVector()).isInAABB(min.toVector(), max.toVector());
			Bukkit.broadcastMessage(String.valueOf(axis));
		}
		if(e.getMessage().startsWith("/tw")){
			Bukkit.broadcastMessage(e.getMessage().split(" ")[1]);
			e.getPlayer().teleport(Bukkit.getWorld(e.getMessage().split(" ")[1]).getSpawnLocation());
		}
	}
	
	public double getAngleMaybeGood(Vector v1, Vector v2){
		float angle = (float) Math.toDegrees(Math.atan2(v1.getZ(), v1.getX()) - Math.atan2(v2.getZ(), v2.getX()));
		if (angle < 0) {
            angle += 360.0F;
        }
        return angle;
	}
	
	public float getAngleOtherMaybeGood(Vector point1, Vector point2) {
        double dx = point2.getX() - point1.getX();
        double dz = point2.getZ() - point1.getZ();
        float angle = (float) Math.toDegrees(Math.atan2(dz, dx)) - 90;
        if (angle < 0) {
            angle += 360.0F;
        }
        return angle;
    }
	
	int a = 0;
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Bukkit.broadcastMessage("Pooooop");
		long time = System.currentTimeMillis();
		Vector newVector = e.getPlayer().getLocation().getDirection().clone();
		Vector randomVector = new Vector(0, 0, 0);
		int invertedAccuracy = 10-1;
		if(invertedAccuracy > 2){
			double smallInverted = (double)invertedAccuracy/22;
			randomVector.setX(Utils.getRandomDouble(-smallInverted, smallInverted));
			randomVector.setY(Utils.getRandomDouble(-smallInverted, smallInverted));
			randomVector.setZ(Utils.getRandomDouble(-smallInverted, smallInverted));
		}
		System.out.println(randomVector.getX() + randomVector.getY() + randomVector.getZ());
		newVector.add(randomVector);
		Location added = e.getPlayer().getEyeLocation();
		for(int a = 0; a<=10; a++){
			e.getPlayer().getWorld().spawnParticle(Particle.FLAME, added, 1, 0, 0, 0, 0);
			e.getPlayer().spigot().playEffect(added, Effect.TILE_DUST, 152, 0, 0, 0, 0, 0, 1, 30);
			added.add(newVector);
		}
		System.out.println(System.currentTimeMillis() - time);
	}
	
	public void rotateAroundAxisY(Vector v, double cos, double sin) {
        double x = v.getX() * cos + v.getZ() * sin;
        double z = v.getX() * -sin + v.getZ() * cos;
        v.setX(x).setZ(z);
    }
	
		
}






