package me.petersoj.data;

public enum Rank {
	
	DEFAULT("default"), PLASMA("plasma"), YOUTUBE("youtube"),
	MOD("mod"), MANAGER("manager"), OWNER("owner");
	
	String teamName;
	
	private Rank(String teamName){
		this.teamName = teamName + "Rank";
	}
	
	public String getTeamName(){
		return this.teamName;
	}

}
