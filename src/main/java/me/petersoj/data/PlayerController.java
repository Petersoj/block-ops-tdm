package me.petersoj.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import me.petersoj.data.database.BlockOpsPlayerDataFetcher;
import me.petersoj.data.database.BlockOpsPlayerDataPusher;
import me.petersoj.game.TDMController;
import me.petersoj.player.BlockOpsPlayer;
import me.petersoj.player.GamePlayer;
import me.petersoj.player.LobbyPlayer;
import me.petersoj.player.SpectatorPlayer;

public class PlayerController {
	
	private TDMController tdmController;
	
	private HashMap<UUID, PlayerData> playerData;
	
	public PlayerController(TDMController tdmController){
		this.tdmController = tdmController;
		playerData = new HashMap<UUID, PlayerData>();
	}
	
	
	public void fetchPlayerData(Player player){
		BlockOpsPlayerDataFetcher fetcher = new BlockOpsPlayerDataFetcher(tdmController, player);
		fetcher.fetch();
	}
	
	public void pushPlayerData(Player player){
		BlockOpsPlayerDataPusher pusher = new BlockOpsPlayerDataPusher(tdmController, tdmController.getPlayerController().getPlayerData(player.getUniqueId()));
		pusher.push();
	}
	
	
	
	public ArrayList<BlockOpsPlayer> getBlockOpsPlayers(){
		ArrayList<BlockOpsPlayer> boPlayers = new ArrayList<>();
		for(PlayerData playerData : this.playerData.values()){
			boPlayers.add(playerData.getBlockOpsPlayer());
		}
		return boPlayers;
	}
	
	public ArrayList<GamePlayer> getGamePlayers(){
		ArrayList<GamePlayer> gamePlayers = new ArrayList<>();
		for(PlayerData playerData : this.playerData.values()){
			if(playerData.getBlockOpsPlayer() instanceof GamePlayer){
				gamePlayers.add((GamePlayer) playerData.getBlockOpsPlayer());
			}
		}
		return gamePlayers;
	}
	
	public ArrayList<LobbyPlayer> getLobbyPlayers(){
		ArrayList<LobbyPlayer> lobbyPlayers = new ArrayList<>();
		for(PlayerData playerData : this.playerData.values()){
			if(playerData.getBlockOpsPlayer() instanceof LobbyPlayer){
				lobbyPlayers.add((LobbyPlayer) playerData.getBlockOpsPlayer());
			}
		}
		return lobbyPlayers;
	}
	
	public ArrayList<SpectatorPlayer> getSpectatorPlayers(){
		ArrayList<SpectatorPlayer> spectatorPlayers = new ArrayList<>();
		for(PlayerData playerData : this.playerData.values()){
			if(playerData.getBlockOpsPlayer() instanceof SpectatorPlayer){
				spectatorPlayers.add((SpectatorPlayer) playerData.getBlockOpsPlayer());
			}
		}
		return spectatorPlayers;
	}
	
	public PlayerData getPlayerData(UUID uuid){
		return this.playerData.get(uuid);
	}
	
	public void addPlayerData(UUID uuid, PlayerData playerData){
		this.playerData.put(uuid, playerData);
	}
	
	public void removePlayerData(UUID uuid){
		this.playerData.remove(uuid);
	}

}
