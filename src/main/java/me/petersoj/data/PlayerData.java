package me.petersoj.data;

import org.bukkit.entity.Player;

import me.petersoj.game.TDMController;
import me.petersoj.player.BlockOpsPlayer;
import me.petersoj.weapon.WeaponClass;

public class PlayerData {
	
	private TDMController tdmController;
	
	private Player player;
	private BlockOpsPlayer boPlayer;
	
	private String[] banOffenseString; // used for punished Players
	private Rank rank;
	private float playerLevel;
	private int prestigeLevel, xp, coins; // +100 xp for every kill, +10 coins for every level up
	private int timePlayed, kills, deaths, wins, losses;
	
	private WeaponClass[] weaponClasses; // persistant data

	public PlayerData(TDMController tdmController, Player player){
		this.tdmController = tdmController;
		this.player = player;
		this.banOffenseString = new String[] {"", "", "", ""};
		this.rank = Rank.DEFAULT;
		this.weaponClasses = new WeaponClass[5];
	}
	
	public float getPlayerLevel(boolean updatePlayerLevel){
		if(updatePlayerLevel){
			this.playerLevel = this.getLevelFromXp(this.xp);
			return playerLevel;
		}else{
			return this.playerLevel;
		}
	}
	
	public float getLevelFromXp(int levelToXP){
		return (float) Math.sqrt((double) (levelToXP/100) / 2 + 1 );
	}
	
	public int getXpFromLevel(float level){
		return (int) (level*level-1)*2;
	}
	
	
	public TDMController getTDMController(){
		return tdmController;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	public void setRank(Rank rank){
		this.rank = rank;
	}

	public int getPrestigeLevel() {
		return prestigeLevel;
	}

	public void incrementPrestigeLevel(int amount) {
		if(this.prestigeLevel < 100){
			this.prestigeLevel += amount;
		}
	}

	public int getXp() {
		return xp;
	}

	public void incrementXp(int xp) {
		if(this.xp < 50000){ // Level 50
			this.xp += xp;
		}
	}

	public int getCoins() {
		return coins;
	}

	public void incrementCoins(int coins) {
		this.coins += coins;
	}

	public int getTimePlayed() {
		return timePlayed;
	}

	public void incrementTimePlayed(int seconds) {
		this.timePlayed += seconds;
	}

	public int getKills() {
		return kills;
	}

	public void incrementKills(int amount) {
		this.kills += amount;
	}

	public int getDeaths() {
		return deaths;
	}

	public void incrementDeaths(int amount) {
		this.deaths += amount;
	}

	public int getWins() {
		return wins;
	}

	public void incrementWins(int amount) {
		this.wins += amount;
	}

	public int getLosses() {
		return losses;
	}

	public void incrementLosses(int amount) {
		this.losses += amount;
	}

	public String[] getBanOffenseString() {
		return banOffenseString;
	}

	public void setBanOffenseString(String[] banOffenseString) {
		this.banOffenseString = banOffenseString;
	}
	
	public WeaponClass[] getWeaponClasses() {
		return weaponClasses;
	}

	public Player getPlayer() {
		return player;
	}
	
	public void setBlockOpsPlayer(BlockOpsPlayer boPlayer){
		this.boPlayer = boPlayer;
	}

	public BlockOpsPlayer getBlockOpsPlayer() {
		return boPlayer;
	}
}
