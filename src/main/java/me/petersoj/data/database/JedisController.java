package me.petersoj.data.database;

import me.petersoj.game.TDMController;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisController {
	
	
	private final JedisPool jedisPool;
	private ServerData serverData;
	
	public JedisController(TDMController tdmController){
		JedisPoolConfig jedisConfig = new JedisPoolConfig();
		jedisConfig.setMaxTotal(25);
		jedisConfig.setMaxWaitMillis(3000);
		jedisConfig.setMinIdle(2);
		jedisConfig.setMaxIdle(10);
		jedisConfig.setSoftMinEvictableIdleTimeMillis(300000);
		// A connection must be idle for at least 5 minutes in order to be eligable for eviction.
		// Evictable thread will run every 30 minutes.
		jedisPool = new JedisPool(jedisConfig, "10.0.1.23", 6379, 3000); // TODO password
		
		serverData = new ServerData(tdmController);
	}
	
	public ServerData getServerData(){
		return serverData;
	}
	
	public JedisPool getJedisPool(){
		return jedisPool;
	}
	
}
