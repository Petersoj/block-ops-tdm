package me.petersoj.data.database;

import org.bukkit.Bukkit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import me.petersoj.BlockOpsTDM;
import me.petersoj.data.PlayerData;
import me.petersoj.game.TDMController;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.gun.Gun;
import redis.clients.jedis.Jedis;

public class BlockOpsPlayerDataPusher implements Runnable{
	
	private TDMController tdmController;
	
	private PlayerData playerData;
	
	public BlockOpsPlayerDataPusher(TDMController tdmController, PlayerData playerData){
		this.tdmController = tdmController;
		this.playerData = playerData;
	}
	
	public void push(){
		Bukkit.getScheduler().runTaskAsynchronously(BlockOpsTDM.getInstance(), this);
	}
	
	@Override
	public void run(){
		Jedis jedis = tdmController.getJedisController().getJedisPool().getResource();
		
		try{
			JSONObject data = new JSONObject();
			data.put("us", "false");
			
			JSONArray banOffenceArray = new JSONArray();
			for(String bo : playerData.getBanOffenseString()){
				banOffenceArray.add(bo);
			}
			data.put("bo", banOffenceArray);
			
			data.put("rk", playerData.getRank().toString());
			data.put("xp", playerData.getXp());
			data.put("pr", playerData.getPrestigeLevel());
			data.put("cns", playerData.getCoins());
			
			JSONArray boughtItems = new JSONArray(); // Bought Items array
//			for(Gun gun : playerData.getWeaponClasses()[0].getGuns()){ // Bought Guns
//				if(gun.getWeaponPossession() == WeaponPossession.BOUGHT || gun.getWeaponPossession() == WeaponPossession.SELECTED){
//					JSONObject gunObject = new JSONObject();
//					gunObject.put("t", "g");
//					gunObject.put("n", gun.getJSONName());
//					gunObject.put("k", gun.getGunKills());
//					JSONArray boughtAttachments = new JSONArray();
//					for(Attachment attachment : gun.getAttachments()){
//						if(attachment.getWeaponPossession() == WeaponPossession.BOUGHT || attachment.getWeaponPossession() == WeaponPossession.SELECTED){
//							boughtAttachments.add(attachment.getJSONName());
//						}
//					}
//					gunObject.put("ba", boughtAttachments);
//					boughtItems.add(gunObject);
//				}
//			}
//			for(Grenade grenade : playerData.getWeaponClasses()[0].getGrenades()){ // Bought Grenades
//				if(grenade.getWeaponPossession() == WeaponPossession.BOUGHT || grenade.getWeaponPossession() == WeaponPossession.SELECTED){
//					JSONObject grenadeObject = new JSONObject();
//					grenadeObject.put("t", "e");
//					grenadeObject.put("n", grenade.getJSONName());
//					boughtItems.add(grenadeObject);
//				}
//			}
//			for(Perk perk : playerData.getWeaponClasses()[0].getPerks()){ // Bought Perks
//				if(perk.getWeaponPossession() == WeaponPossession.BOUGHT || perk.getWeaponPossession() == WeaponPossession.SELECTED){
//					JSONObject perkObject = new JSONObject();
//					perkObject.put("t", "p");
//					perkObject.put("n", perk.getJSONName());
//					boughtItems.add(perkObject);
//				}
//			}
			data.put("bi", boughtItems);
			
			JSONArray weaponClasses = new JSONArray(); // weapon glasses array
			for(WeaponClass weaponClass : playerData.getWeaponClasses()){
				if(weaponClass == null){
					continue;
				}
				JSONArray classArray = new JSONArray();
				
				JSONObject primaryObject = new JSONObject();
				Gun primaryGun = weaponClass.getPrimary();
				primaryObject.put("g", primaryGun.getJSONName());
				primaryObject.put("a1", (primaryGun.getAttachment1() == null ? "" : primaryGun.getAttachment1().getJSONName()));
				primaryObject.put("a2", (primaryGun.getAttachment2() == null ? "" : primaryGun.getAttachment2().getJSONName()));
				classArray.add(primaryObject);
				
				JSONObject secondaryObject = new JSONObject();
				Gun secondaryGun = weaponClass.getSecondary();
				secondaryObject.put("g", secondaryGun.getJSONName());
				secondaryObject.put("a1",  (secondaryGun.getAttachment1() == null ? "" : secondaryGun.getAttachment1().getJSONName()));
				secondaryObject.put("a2", (secondaryGun.getAttachment2() == null ? "" : secondaryGun.getAttachment2().getJSONName()));
				classArray.add(secondaryObject);
				
				classArray.add(weaponClass.getLethal().getJSONName());
				classArray.add(weaponClass.getTactical().getJSONName());
				classArray.add(weaponClass.getPerk1().getJSONName());
				classArray.add(weaponClass.getPerk2().getJSONName());
				classArray.add(weaponClass.getPerk3().getJSONName());
				
				weaponClasses.add(classArray);
			}
			data.put("cls", weaponClasses);
			
			data.put("tp", playerData.getTimePlayed());
			data.put("kl", playerData.getKills());
			data.put("dth", playerData.getDeaths());
			data.put("wn", playerData.getWins());
			data.put("ls", playerData.getLosses());
			
			jedis.set(playerData.getPlayer().getUniqueId().toString(), data.toJSONString());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(jedis != null){
				jedis.close();
			}
			if(tdmController.isGameInSession()){
				tdmController.getGameController().onPlayerDataPushed(playerData);
			}else{
				tdmController.getLobbyController().onPlayerDataPushed(playerData);
			}
		}
	}
}