package me.petersoj.data.database;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import me.petersoj.BlockOpsTDM;
import me.petersoj.data.PlayerData;
import me.petersoj.data.Rank;
import me.petersoj.game.TDMController;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.attachment.Attachment;
import me.petersoj.weapon.grenade.Grenade;
import me.petersoj.weapon.gun.Gun;
import me.petersoj.weapon.perk.Perk;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class BlockOpsPlayerDataFetcher implements Runnable{
	
	private TDMController tdmController;
	
	private Player player;
	private BukkitTask task;
	
	private PlayerData playerData;
	
	private Jedis jedis;
	private int tries;
	private boolean retry;
	
	public BlockOpsPlayerDataFetcher(TDMController tdmController, Player player){
		this.tdmController = tdmController;
		this.player = player;
		retry = true;
	}
	
	public void fetch(){
		task = Bukkit.getScheduler().runTaskTimerAsynchronously(BlockOpsTDM.getInstance(), this, 0, 3);
	}
	
	public void run(){
		try{
			if(jedis == null){
				jedis = tdmController.getJedisController().getJedisPool().getResource();
			}
			String json = jedis.get(player.getUniqueId().toString());
			if(json == null || json.equals("")){
				retry = true;
				return;
			}
			
			JSONObject data = (JSONObject) new JSONParser().parse(json);
			
			if(Boolean.parseBoolean((String) data.get("us"))){ // Another server is using the data
				retry = true;
				return; // go to finally block
			}
			
			retry = false; // we know have the good data
			playerData = new PlayerData(tdmController, player);
			
			JSONArray banOffenceArray = (JSONArray) data.get("bo");
			String[] banOffence = (String[]) banOffenceArray.toArray(new String[banOffenceArray.size()]); // banOffence
			playerData.setBanOffenseString(banOffence);
			if(banOffence[0].equals("t")){ // time
				if(Long.parseLong(banOffence[1]) > System.currentTimeMillis()){ // Still tempBanned
					kickPlayerDataProblem(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You have been temporarily banned.\n"
							+ ChatColor.RED + "Offence: " + ChatColor.WHITE + banOffence[2] + ".\n"
							+ ChatColor.GRAY + "Reason: " + ChatColor.WHITE + banOffence[3] + ".\n"
							+ ChatColor.GOLD + "Time of Unban: " + ChatColor.WHITE 
							+ new SimpleDateFormat("MMMMMMMMMM dd, yyyy").format(new Date(Long.parseLong(banOffence[1]))));
					return;
				}else{
					Arrays.fill(playerData.getBanOffenseString(), ""); // they are unbanned
				}
			}else if(banOffence[0].equals("p")){ // permanent
				kickPlayerDataProblem(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You have been permanently banned.\n"
					+ ChatColor.RED + "Offence: " + ChatColor.WHITE + banOffence[2] + "\n"
					+ ChatColor.GRAY + "Reason: " + ChatColor.WHITE + banOffence[3]);
				return;
			}
			playerData.setRank(Rank.valueOf((String) data.get("rk"))); // Rank
			playerData.incrementXp(((Long) data.get("xp")).intValue()); // XP
			playerData.incrementPrestigeLevel(((Long) data.get("pr")).intValue());
			playerData.incrementCoins(((Long) data.get("cns")).intValue()); // Tokens
			
			JSONArray classes = (JSONArray) data.get("cls"); // Classes instantiation
			for(int classNumber = 0; classNumber < classes.size(); classNumber++){
				playerData.getWeaponClasses()[classNumber] = new WeaponClass(playerData, "Class " + classNumber, classNumber);
			}
			
			int playerLevel = (int) playerData.getPlayerLevel(true);
			
			JSONArray boughtItems = (JSONArray) data.get("bi"); // BoughtItems
			for(int index = 0; index < boughtItems.size(); index++){
				JSONObject boughtItem = (JSONObject) boughtItems.get(index);
				String boughtItemType = (String) boughtItem.get("t"); // Type to determine layout of the object
				String boughtItemName = (String) boughtItem.get("n"); // Name/character of bought Item
				boolean boughtWithRealMoney = boughtItem.containsKey("b");
				for(WeaponClass gunClass : playerData.getWeaponClasses()){
					if(gunClass == null){
						continue;
					}
					if(boughtItemType.equals("g")){ // Gun Bought Item
						for(Gun gun : gunClass.getGuns()){
							gun.setBoughtWithRealMoney(boughtWithRealMoney);
							if(gun.getJSONName().equals(boughtItemName)){
								gun.setWeaponPossession(WeaponPossession.BOUGHT);
								gun.setGunKills(((Long) data.get("k")).intValue());
								
								JSONArray boughtAttachments = (JSONArray) boughtItem.get("ba"); // bought attachments
								for(Attachment attachment : gun.getAttachments()){ // Gun default attachments
									for(int attachIndex = 0; attachIndex < boughtAttachments.size(); attachIndex++){
										if(attachment.getJSONName().equals((String) boughtAttachments.get(attachIndex))){
											attachment.setWeaponPossession(WeaponPossession.BOUGHT);
										}else if(gun.getGunLevel() > attachment.getUnlockLevel()){ // attachment unlock level is less than Gun Weapon Level
											attachment.setWeaponPossession(WeaponPossession.UNBOUGHT);
										}
									}
								}
							}else if(playerLevel > gun.getUnlockLevel()){ // check for unlockable
								gun.setWeaponPossession(WeaponPossession.UNBOUGHT);
							}
						}
					}else if(boughtItemType.equals("gn")){ // Grenade Bought Item
						for(Grenade grenade : gunClass.getGrenades()){
							grenade.setBoughtWithRealMoney(boughtWithRealMoney);
							if(grenade.getJSONName().equals(boughtItemName)){
								grenade.setWeaponPossession(WeaponPossession.BOUGHT);
							}else if(playerLevel > grenade.getUnlockLevel()){ // check for unlockable
								grenade.setWeaponPossession(WeaponPossession.UNBOUGHT);
							}
						}
					}else if(boughtItemType.equals("pk")){ // Perk Bought Item
						for(Perk perk : gunClass.getPerks()){
							perk.setBoughtWithRealMoney(boughtWithRealMoney);
							if(perk.getJSONName().equals(boughtItemName)){
								perk.setWeaponPossession(WeaponPossession.BOUGHT);
							}else if(playerLevel > perk.getUnlockLevel()){ // check for unlockable
								perk.setWeaponPossession(WeaponPossession.UNBOUGHT);
							}
						}
					}
				}
			}
			
			// Classes loop
			for(int classNumber = 0; classNumber < classes.size(); classNumber++){
				JSONArray classArray = (JSONArray) classes.get(classNumber); // Current loop Class array
				WeaponClass gunClass = playerData.getWeaponClasses()[classNumber];
				JSONObject primaryObject = (JSONObject) classArray.get(0);
				JSONObject secondaryObject = (JSONObject) classArray.get(1);
				String primaryJSONName = (String) primaryObject.get("g");
				String secondaryJSONName = (String) secondaryObject.get("g");
				String primaryAttachment1 = (String) primaryObject.get("a1");
				String primaryAttachment2 = (String) primaryObject.get("a2");
				String secondaryAttachment1 = (String) secondaryObject.get("a1");
				String secondaryAttachment2 = (String) secondaryObject.get("a2");
				for(Gun gun : gunClass.getGuns()){ // Class Gun (Primary, secondary) and attachments
					if(gun.getJSONName().equals(primaryJSONName)){
						gunClass.setPrimary(gun);
						gun.setWeaponPossession(WeaponPossession.SELECTED);
						for(Attachment attachment : gun.getAttachments()){ // Selected Attachment
							if(attachment.getJSONName().equals(primaryAttachment1)){
								attachment.setWeaponPossession(WeaponPossession.SELECTED);
								gun.setAttachment1(attachment);
							}else if(attachment.getJSONName().equals(primaryAttachment2)){
								attachment.setWeaponPossession(WeaponPossession.SELECTED);
								gun.setAttachment2(attachment);
							}
						}
					}else if(gun.getJSONName().equals(secondaryJSONName)){
						gunClass.setSecondary(gun);
						gun.setWeaponPossession(WeaponPossession.SELECTED);
						for(Attachment attachment : gun.getAttachments()){ // Selected Attachment
							if(attachment.getJSONName().equals(secondaryAttachment1)){
								attachment.setWeaponPossession(WeaponPossession.SELECTED);
								gun.setAttachment1(attachment);
							}else if(attachment.getJSONName().equals(secondaryAttachment2)){
								attachment.setWeaponPossession(WeaponPossession.SELECTED);
								gun.setAttachment2(attachment);
							}
						}
					}
				}
				String lethalJSONName = (String) classArray.get(2);
				String tacticalJSONName = (String) classArray.get(3);
				for(Grenade grenade : gunClass.getGrenades()){ // Class Grenades
					if(grenade.getJSONName().equals(lethalJSONName)){
						grenade.setWeaponPossession(WeaponPossession.SELECTED);
						gunClass.setLethal(grenade);
					}else if(grenade.getJSONName().equals(tacticalJSONName)){
						grenade.setWeaponPossession(WeaponPossession.SELECTED);
					}
				}
				
				String perk1JSONName = (String) classArray.get(4);
				String perk2JSONName = (String) classArray.get(5);
				String perk3JSONName = (String) classArray.get(6);
				for(Perk perk : gunClass.getPerks()){ // Class Perks
					if(perk.equals(perk1JSONName)){
						perk.setWeaponPossession(WeaponPossession.SELECTED);
						gunClass.setPerk1(perk);
					}else if(perk.equals(perk2JSONName)){
						perk.setWeaponPossession(WeaponPossession.SELECTED);
						gunClass.setPerk2(perk);
					}else if(perk.equals(perk3JSONName)){
						perk.setWeaponPossession(WeaponPossession.SELECTED);
						gunClass.setPerk3(perk);
					}
				}
			}
			
			playerData.incrementTimePlayed(((Long) data.get("tp")).intValue()); // Time Played
			playerData.incrementKills(((Long) data.get("kl")).intValue()); // Kills
			playerData.incrementDeaths(((Long) data.get("dth")).intValue()); // Deaths
			playerData.incrementWins(((Long) data.get("wn")).intValue()); // Wins
			playerData.incrementLosses(((Long) data.get("ls")).intValue()); // Losses
		}catch(JedisException e){
			// could not connect, or wrong auth, or no key in redis
			e.printStackTrace();
			retry = false;
			tries = -1;
		}catch(ParseException e) {
			// json couldn't be converted from string
			e.printStackTrace();
			retry = false;
			tries = -1;
		}catch(ClassCastException e){
			// coulnd't parse json from inside
			e.printStackTrace();
			retry = false;
			tries = -1;
		}catch(NullPointerException e){
			// something was null :(
			e.printStackTrace();
			retry = false;
			tries = -1;
		}finally{
			if(tries > 5){ // this will only happen if their data is not up to date
				this.stop();
			}else if(!retry){ // this will only happen if tries is less than 5 and no retry
				this.stop();
			}else{ // tries is less and 5 and we are going to retry
				tries++;
			}
//			if(retry){
//				
//			}else{
//				task.cancel();
//				if(jedis != null){
//					jedis.close();
//				}
//				if(tries > 0 && playerData != null){
//					this.tasksAfterFetched();
//				}else{
//					kickPlayerDataProblem("");
//				}
//			}
//			if(tries > 5){
//				if(jedis != null){
//					jedis.close();
//				}
//			}
//			if(tries > 5 || !retry){ // tries will be -1 when exceptions happen
//				
//			}else{
//				tries++;
//			}
		}
	}
	
	private void stop(){
		task.cancel();
		if(jedis != null){
			jedis.close();
		}
		if(playerData != null){
			this.tasksAfterFetched();
		}else{
			kickPlayerDataProblem("");
		}
	}
	
	private void kickPlayerDataProblem(String message){
		new BukkitRunnable() {
			@Override
			public void run() {
				if(message.equals("")){
					playerData.getPlayer().kickPlayer(ChatColor.RED + "There was a problem fetching you're user data.\nPlease re-log. :)");
				}else{
					playerData.getPlayer().kickPlayer(message);
				}
			}
		}.runTask(BlockOpsTDM.getInstance());
	}
	
	private void tasksAfterFetched(){
		if(tdmController.isGameInSession()){
			tdmController.getGameController().onPlayerDataFetched(playerData);
		}else{
			tdmController.getLobbyController().onPlayerDataFetched(playerData);
		}
	}
}