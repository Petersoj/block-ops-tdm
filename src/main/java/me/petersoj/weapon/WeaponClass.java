package me.petersoj.weapon;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.petersoj.data.PlayerData;
import me.petersoj.util.Utils;
import me.petersoj.weapon.grenade.Grenade;
import me.petersoj.weapon.gun.Gun;
import me.petersoj.weapon.perk.Perk;

public class WeaponClass {
	
	private PlayerData playerData;
	
	private String name; // implements Customizable names later
	private ItemStack classItemStack;

	private List<Gun> guns;
	private List<Grenade> grenades;
	private List<Perk> perks;
	
	private Gun primary, secondary;
	private Grenade tactical, lethal;
	private Perk perk1, perk2, perk3;
	
	public WeaponClass(PlayerData playerData, String name, int classNumber){
		this.playerData = playerData;
		this.name = name;
		
		// TODO Accurate below
		if(classNumber == 0){
			this.classItemStack = new ItemStack(Material.IRON_AXE, 1, (short) 4); // "1" class itemStack
		}else if(classNumber == 1){
			this.classItemStack = new ItemStack(Material.IRON_AXE, 1, (short) 4); // "2" class itemStack
		}else if(classNumber == 2){
			this.classItemStack = new ItemStack(Material.IRON_AXE, 1, (short) 4); // "3" class itemStack
		}else if(classNumber == 3){
			this.classItemStack = new ItemStack(Material.IRON_AXE, 1, (short) 4); // "4" class itemStack
		}else if(classNumber == 4){
			this.classItemStack = new ItemStack(Material.IRON_AXE, 1, (short) 4); // "5" class itemStack
		}

		this.guns = new ArrayList<Gun>();
		this.grenades = new ArrayList<Grenade>();
		this.perks = new ArrayList<Perk>();
		
		// TODO Add all the default guns, grenades, and perks to lists.
		
	}
	
	public void setItemLore(){
		List<String> lore = new ArrayList<String>();
		
		Utils.setNameAndLore(classItemStack, ChatColor.GOLD + "" + ChatColor.BOLD + name, lore);
	}
	
	public void setIsDefaultClass(){
		for(Gun gun : this.getGuns()){
			if(gun.isDefaultWeapon()){
				gun.setWeaponPossession(WeaponPossession.SELECTED);
			}else if(gun.isBoughtWithRealMoney()){
				gun.setWeaponPossession(WeaponPossession.BOUGHT);
			}else{
				gun.setWeaponPossession(WeaponPossession.LOCKED);
			}
		}
		for(Grenade grenade : this.getGrenades()){
			if(grenade.isDefaultWeapon()){
				grenade.setWeaponPossession(WeaponPossession.SELECTED);
			}else if(!grenade.isBoughtWithRealMoney()){
				grenade.setWeaponPossession(WeaponPossession.BOUGHT);
			}else{
				grenade.setWeaponPossession(WeaponPossession.LOCKED);
			}
		}
		for(Perk perk : this.getPerks()){
			if(perk.isDefaultWeapon()){
				perk.setWeaponPossession(WeaponPossession.SELECTED);
			}else if(!perk.isBoughtWithRealMoney()){
				perk.setWeaponPossession(WeaponPossession.BOUGHT);
			}else{
				perk.setWeaponPossession(WeaponPossession.LOCKED);
			}
		}
	}
	
	
	
	public PlayerData getPlayerData() {
		return playerData;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ItemStack getClassItemStack() {
		return classItemStack;
	}

	public List<Gun> getGuns() {
		return guns;
	}

	public List<Grenade> getGrenades() {
		return grenades;
	}

	public List<Perk> getPerks() {
		return perks;
	}

	public Gun getPrimary() {
		return primary;
	}

	public void setPrimary(Gun primary) {
		this.primary = primary;
	}

	public Gun getSecondary() {
		return secondary;
	}

	public void setSecondary(Gun secondary) {
		this.secondary = secondary;
	}

	public Grenade getTactical() {
		return tactical;
	}

	public void setTactical(Grenade tactical) {
		this.tactical = tactical;
	}

	public Grenade getLethal() {
		return lethal;
	}

	public void setLethal(Grenade lethal) {
		this.lethal = lethal;
	}

	public Perk getPerk1() {
		return perk1;
	}

	public void setPerk1(Perk perk1) {
		this.perk1 = perk1;
	}

	public Perk getPerk2() {
		return perk2;
	}

	public void setPerk2(Perk perk2) {
		this.perk2 = perk2;
	}

	public Perk getPerk3() {
		return perk3;
	}

	public void setPerk3(Perk perk3) {
		this.perk3 = perk3;
	}
}
