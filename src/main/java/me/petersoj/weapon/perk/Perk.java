package me.petersoj.weapon.perk;

import java.util.ArrayList;
import java.util.List;

import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public abstract class Perk extends Weapon{
	
	private PerkType perkType;

	public Perk(WeaponClass weaponClass, String name){
		super(weaponClass, name);
	}

	public void createItemStack(ItemStack base) {
		List<String> lore = new ArrayList<String>();
		lore.add(" ");
		for(String descriptionLine : super.getDesciption().split("\n")){
			lore.add(ChatColor.WHITE + descriptionLine);
		}
		//super.setNameAndLore(ChatColor.GREEN + "" + ChatColor.UNDERLINE + ChatColor.BOLD + super.name, lore);
		//super.itemBase = base;
	}
	
	// TIER 1
	// Fast Hands, Marathon, Overkill - two primarys
	// LATER - Blind Eye - Everything except UAV HATR, Scavenger
	// TIER 2
	// Flak Jacket, Lightweight
	// LATER - Hardline - -1 killstreak, Assassin - Invis UAV HAtR, Tracker, Gung-Ho - Shoot while sprinting
	// TIER 3
	// Descend - Take no fall damage, Metal Stock - punches does increased damage, Rails
	// LATER - Tactical Mask, Vest - have extra grenades
	
	
	public PerkType getPerkType(){
		return perkType;
	}

}
