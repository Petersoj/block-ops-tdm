package me.petersoj.weapon.grenade;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;

public abstract class Grenade extends Weapon{
	
	public GrenadeType grenadeType;
	public String visualCharacter;

	public Grenade(WeaponClass weaponClass, String name) {
		super(weaponClass, name);
	}
	
	public void createItemStack(ItemStack base) {
		List<String> lore = new ArrayList<String>();
		lore.add(" ");
		for(String descriptionLine : super.getDesciption().split("\n")){
			lore.add(ChatColor.WHITE + descriptionLine);
		}
		//super.setNameAndLore(ChatColor.GREEN + "" + ChatColor.UNDERLINE + ChatColor.BOLD + super.name, lore);
		//super.itemBase = base;
	}
	
	// LETHAL
	// Frag, Semtex
	//
	// TACTICAL
	// Concussion Grenade, Flash Grenade
	
	public GrenadeType getGrenadeType(){
		return grenadeType;
	}

}
