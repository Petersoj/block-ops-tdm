package me.petersoj.weapon.attachment;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.gun.Gun;

public abstract class Attachment extends Weapon {
	
	private Gun attachedGun;

	public Attachment(WeaponClass weaponClass, Gun attachedGun, String name) {
		super(weaponClass, name);
		this.attachedGun = attachedGun;
	}
	
	public void createItemStack(ItemStack base){
		// The player will never have this ItemStack in their hand. EXCEPT FOR RedDotSight
		//super.setNameAndLore("", Arrays.asList("Umm, how are you seeing this?"));
		//super.itemBase = base;
	}

	
	
	// http://www.theblackopsiii.com/bo3/weapons-list/attachments/
	// $ Red Dot Sight $ - Add red dot sight when ADS
	// High Caliber - Headshots increased Damage
	// $ Quickdraw $ - ADS Faster
	// $ Extended Mags $ - More bullets per mag
	// $ Fast Mags $ - Change mags quicker
	// Rapid Fire - Actually increase Fire Rate
	// Grip - Actually increase Accuracy
	// $ Long barrel $ - Actually Increase Range
	// Flamethrower ?? :P
	
	
	public Gun getAttachedGun() {
		return attachedGun;
	}

	public void setAttachedGun(Gun attachedGun) {
		this.attachedGun = attachedGun;
	}
}
