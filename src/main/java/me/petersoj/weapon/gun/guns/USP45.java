package me.petersoj.weapon.gun.guns;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import me.petersoj.util.Utils;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.gun.Gun;
import me.petersoj.weapon.gun.GunType;

public class USP45 extends Gun{
	
	public USP45(WeaponClass weaponClass){
		super(weaponClass, "USP .45");
		
		// TODO accurate values
		super.setDesciption("Your general purpose pistol.");
		super.setJSONName("P45");
		super.setBoughtWithRealMoney(false);
		super.setDefaultWeapon(true);
		super.setPrice(0);
		super.setUnlockLevel(0);
		
		super.setGunType(GunType.PISTOL);
		super.setVisualCharacter("㐂");
		super.setHudBarSpacingString("㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀");
		super.setAccuracy(6); super.setDamage(3); super.setRange(4); super.setFireRate(3); super.setMobility(0);
		super.setMaxBullets(6); super.setMaxMags(3); super.setReloadTime(30); super.setCockTime(7);
		super.setSoundHammerClick(Sound.MUSIC_END);
		super.setSoundShoot(Sound.MUSIC_END); super.setSoundCock(Sound.MUSIC_END);
		super.setSoundMagIn(Sound.MUSIC_END); super.setSoundMagOut(Sound.MUSIC_END);
		super.setFirstPersonYaw(8.2); super.setFirstPersonPitch(4.5); super.setFirstPersonLength(0.4);
		super.setThirdPersonYaw(8.2); super.setThirdPersonPitch(4.5); super.setThirdPersonLength(1.5);
		
		super.setBullets(super.getMaxBullets()); super.setBulletsTotal(super.getMaxBullets() * (super.getMaxMags() - 1)); // Total bullets are separte from current magazine bullets
		
//		ArrayList<Attachment> attachments = super.getAttachments();
//		attachments.add(new ExtendsMags()); // TODO add more attachments
		
	}
	
	@Override
	public void setWeaponPossession(WeaponPossession weaponPossession) {
		super.setWeaponPossession(weaponPossession);
		if(weaponPossession != WeaponPossession.LOCKED){
			// TODO accurate values
			super.setItemStackBase(new ItemStack(Material.DIAMOND_PICKAXE, 1, (short) 4)); // The gun model
			if(weaponPossession == WeaponPossession.SELECTED){
				super.setDefaultSightItem(new ItemStack(Material.DIAMOND_AXE, 1, (short) 5));
				super.setBowItem(new ItemStack(Material.DIAMOND_AXE, 1, (short) 5));
				Utils.setItemStackBlank(super.getDefaultSightItem(), super.getBowItem());
			}
		}
		super.setBaseLore(); // Sets the lore of life
	}
}