package me.petersoj.weapon.gun;

public enum GunType {
	
	SMG("Submachine Gun"), ASSAULT_RIFLE("Assault Rifle"), SHOTGUN("Shotgun"),
	SNIPER_RIFLE("Sniper Rifle"), PISTOL("Pistol"), SPECIAL("Special"); 
	
	private String typeName;
	
	private GunType(String typeName){
		this.typeName = typeName;
	}
	
	public String getTypeName(){
		return typeName;
	}

}
