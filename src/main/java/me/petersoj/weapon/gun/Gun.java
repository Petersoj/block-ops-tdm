package me.petersoj.weapon.gun;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import me.petersoj.BlockOpsTDM;
import me.petersoj.player.GamePlayer;
import me.petersoj.util.Utils;
import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.attachment.Attachment;

public abstract class Gun extends Weapon{
	
	private GunType gunType;
	private String visualCharacter;
	private String hudBarSpacingString;
	private int accuracy, damage, range, fireRate, mobility, maxBullets, maxMags, reloadTime, cockTime;
	private Sound soundHammerClick, soundShoot, soundCock, soundMagIn, soundMagOut;
	private double firstPersonYaw, firstPersonPitch, firstPersonLength, thirdPersonYaw, thirdPersonPitch, thirdPersonLength;
	private int extraDamage, extraAccuracy, extraRange, extraFireRate;
	private int bullets, bulletsTotal;
	private boolean reloading, changedItemWhileReloading;
	private boolean selected, cooldown;
	private Attachment attachment1,attachment2;
	private ItemStack defaultSightItem;
	private ItemStack bowItem;
	private ArrayList<Attachment> attachments;
	private int gunKills;
	
	// http://www.themodernwarfare2.com/mw2/multiplayer/weapons/
	
	// https://richardpmurfin.files.wordpress.com/2011/11/gun-stats1.jpeg <- GUN STATS
	// Assault Rifles - M4A1, ACR
	// SMG - UMP45, P90
	// // LMG - MG4 - DO LMG's LATER
	// Sniper - Intervention, Barret .50cal
	// Shotguns - SPAS-12, Striker
	// Handguns - USP .45, .44 Magnum, G18
	// Launchers - AT4-HS, RPG-7
	
	public Gun(WeaponClass weaponClass, String name){
		super(weaponClass, name);
		this.attachments = new ArrayList<Attachment>();
	}
	
	public boolean shootBullet(GamePlayer gamePlayer){
		Player player = gamePlayer.getPlayer();
		if(reloading){
			return false;
		}
		if(bullets <= 0){
			player.playSound(player.getEyeLocation(), this.soundHammerClick, 2, 1);
			return false;
		}
		
		
		return true;
	}
	
	public void reload(GamePlayer gamePlayer){
		boolean cock = false;
		if(reloading){
			return;
		}
		reloading = true;
		if(this.bullets <= 0){
			
		}else{
			cock = true;
		}
		gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getEyeLocation(), soundMagOut, 2, 1);
		new BukkitRunnable() {
			@Override
			public void run() {
				if(reloading && !changedItemWhileReloading){
					gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getEyeLocation(), soundMagIn, 2, 1);
					bullets = maxBullets;
					if(!false/* clock */){
						reloading = false;
					}
				}
			}
		}.runTaskLater(BlockOpsTDM.getInstance(), this.reloadTime);
		if(cock){
			new BukkitRunnable() {
				@Override
				public void run() {
					if(reloading){
						gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getEyeLocation(), soundCock, 2, 1);
						reloading = false; // TODO on Hand switch to this gun, set reloading to false and changedItemReloading to true
					}
				}
			}.runTaskLater(BlockOpsTDM.getInstance(), this.reloadTime + 5); // TODO check for good cock sound timing.
		}
	}
	
	public void flashMuzzle(GamePlayer gamePlayer){
		Location firstPersonLoc = gamePlayer.getPlayer().getEyeLocation();
		firstPersonLoc.setYaw((float) (firstPersonLoc.getYaw() + this.firstPersonYaw));
		firstPersonLoc.setPitch((float) (firstPersonLoc.getPitch() + this.firstPersonPitch));
		Location firstPersonParticle = firstPersonLoc.getDirection().multiply(firstPersonLength).toLocation(firstPersonLoc.getWorld());
		if(gamePlayer.isSneaking()){
			firstPersonLoc.add(0, -0.2, 0); // TODO check if accurate
		}
		gamePlayer.getPlayer().spawnParticle(Particle.WATER_BUBBLE, firstPersonParticle.add(firstPersonLoc), 1);
		
		Location thirdPersonLoc = gamePlayer.getPlayer().getEyeLocation();
		thirdPersonLoc.setYaw((float) (thirdPersonLoc.getYaw() + this.thirdPersonYaw));
		thirdPersonLoc.setPitch((float) (thirdPersonLoc.getPitch() + this.thirdPersonPitch));
		Location thirdPersonParticle = thirdPersonLoc.getDirection().multiply(thirdPersonLength).toLocation(thirdPersonLoc.getWorld());
		for(Player other : Bukkit.getOnlinePlayers()){
			if(other != gamePlayer.getPlayer()){
				other.spawnParticle(Particle.WATER_BUBBLE, thirdPersonParticle, 1);
			}
		}
	}
	
	public Vector getRandomBulletVectorWithAccuracy(Vector playerDirection){
		Vector newVector = playerDirection.clone();
		Vector randomVector = new Vector(0, 0, 0);
		int invertedAccuracy = 10-accuracy+extraAccuracy;
		if(invertedAccuracy > 2){
			double max = (double) invertedAccuracy/22;
			double min = -max;
			randomVector.setX(Utils.getRandomDouble(min, max));
			randomVector.setY(Utils.getRandomDouble(min, max));
			randomVector.setZ(Utils.getRandomDouble(min, max));
		}
		return newVector.add(randomVector);
	}
	
	public Block[] getBulletBlocksIntersected(Location start, Vector normalizedDirection, int length){
		Block[] blocks = new Block[2];
		BlockIterator bi = new BlockIterator(start.getWorld(), start.toVector(), normalizedDirection, 0, length);
		Block previous = null;
		while(bi.hasNext()){
			Block current = bi.next();
			if(current.getType() != Material.AIR){
				blocks[0] = previous;
				blocks[1] = current;
				break;
			}
			previous = current;
		}
		return blocks;
	}
	
	@Override
	public void setBaseLore() {
		super.setBaseLore();
		List<String> lore = super.getItemStackBase().getItemMeta().getLore();
		lore.add(" ");
		lore.add(ChatColor.GRAY + "Type: " + ChatColor.WHITE + this.gunType.getTypeName());
		if(super.getWeaponPossession() == WeaponPossession.SELECTED){
			lore.add(" ");
			lore.add(ChatColor.GRAY + "Attachment: " + ChatColor.RED + ((attachment1 == null) ? "None" : attachment1.getName()));
			if(attachment2 != null){
				lore.add(ChatColor.GRAY + "Second Attachment: " + ChatColor.RED + attachment2.getName());
			}
		}
		lore.add(" ");
		if(super.getWeaponPossession() == WeaponPossession.BOUGHT || super.getWeaponPossession() == WeaponPossession.SELECTED){
			lore.add(ChatColor.WHITE + "Gun Level: " + ChatColor.YELLOW + this.getGunLevel());
			lore.add(" ");
		}
		lore.add(this.getStatisticString("Accuracy", accuracy, extraAccuracy));
		lore.add(this.getStatisticString("Damage", damage, extraDamage));
		lore.add(this.getStatisticString("Range", range, extraRange));
		lore.add(this.getStatisticString("Fire Rate", fireRate, extraFireRate));
		if(gunType != GunType.SPECIAL && gunType != GunType.PISTOL){
			lore.add(this.getStatisticString("Mobility", mobility, 0));
		}
		super.getItemStackBase().getItemMeta().setLore(lore); // Only need to set the lore
	}
	private String getStatisticString(String name, int stat, int extraStat){
		StringBuilder builder = new StringBuilder(ChatColor.WHITE.toString());
		for(int a = 1; a<= 10; a++){
			if(a <= stat){
				builder.append("█");
			}else{
				if(a > stat && a <= extraStat+stat){
					builder.append(ChatColor.GREEN + "");
				}
				if(a > stat+extraStat){
					builder.append(ChatColor.DARK_GRAY + "");
				}
				builder.append("█");
			}
		}
		builder.append(ChatColor.WHITE + " " + name);
		return builder.toString();
	}
	
	public float getGunLevel(){ // Max gun level is 15 and requires 450 kills.
		DecimalFormat form = new DecimalFormat("#0.#");
		if(this.gunKills > 450){
			return 15f;
		}else{
			return Float.valueOf(form.format(Math.sqrt((double) ((int) this.gunKills / 2 ) + 1)));
		}
	}
	

	public GunType getGunType() {
		return gunType;
	}

	public void setGunType(GunType gunType) {
		this.gunType = gunType;
	}

	public String getVisualCharacter() {
		return visualCharacter;
	}

	public void setVisualCharacter(String visualCharacter) {
		this.visualCharacter = visualCharacter;
	}

	public String getHudBarSpacingString() {
		return hudBarSpacingString;
	}

	public void setHudBarSpacingString(String hudBarSpacingString) {
		this.hudBarSpacingString = hudBarSpacingString;
	}

	public int getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getFireRate() {
		return fireRate;
	}

	public void setFireRate(int fireRate) {
		this.fireRate = fireRate;
	}

	public int getMobility() {
		return mobility;
	}

	public void setMobility(int mobility) {
		this.mobility = mobility;
	}

	public int getMaxBullets() {
		return maxBullets;
	}

	public void setMaxBullets(int maxBullets) {
		this.maxBullets = maxBullets;
	}

	public int getMaxMags() {
		return maxMags;
	}

	public void setMaxMags(int maxMags) {
		this.maxMags = maxMags;
	}

	public int getReloadTime() {
		return reloadTime;
	}

	public void setReloadTime(int reloadTime) {
		this.reloadTime = reloadTime;
	}

	public int getCockTime() {
		return cockTime;
	}

	public void setCockTime(int cockTime) {
		this.cockTime = cockTime;
	}
	
	public Sound getSoundHammerClick(){
		return soundHammerClick;
	}
	
	public void setSoundHammerClick(Sound soundHammerClick){
		this.soundHammerClick = soundHammerClick;
	}

	public Sound getSoundShoot() {
		return soundShoot;
	}

	public void setSoundShoot(Sound soundShoot) {
		this.soundShoot = soundShoot;
	}

	public Sound getSoundCock() {
		return soundCock;
	}

	public void setSoundCock(Sound soundCock) {
		this.soundCock = soundCock;
	}

	public Sound getSoundMagIn() {
		return soundMagIn;
	}

	public void setSoundMagIn(Sound soundMagIn) {
		this.soundMagIn = soundMagIn;
	}

	public Sound getSoundMagOut() {
		return soundMagOut;
	}

	public void setSoundMagOut(Sound soundMagOut) {
		this.soundMagOut = soundMagOut;
	}

	public double getFirstPersonYaw() {
		return firstPersonYaw;
	}

	public void setFirstPersonYaw(double firstPersonYaw) {
		this.firstPersonYaw = firstPersonYaw;
	}

	public double getFirstPersonPitch() {
		return firstPersonPitch;
	}

	public void setFirstPersonPitch(double firstPersonPitch) {
		this.firstPersonPitch = firstPersonPitch;
	}

	public double getFirstPersonLength() {
		return firstPersonLength;
	}

	public void setFirstPersonLength(double firstPersonLength) {
		this.firstPersonLength = firstPersonLength;
	}

	public double getThirdPersonYaw() {
		return thirdPersonYaw;
	}

	public void setThirdPersonYaw(double thirdPersonYaw) {
		this.thirdPersonYaw = thirdPersonYaw;
	}

	public double getThirdPersonPitch() {
		return thirdPersonPitch;
	}

	public void setThirdPersonPitch(double thirdPersonPitch) {
		this.thirdPersonPitch = thirdPersonPitch;
	}

	public double getThirdPersonLength() {
		return thirdPersonLength;
	}

	public void setThirdPersonLength(double thirdPersonLength) {
		this.thirdPersonLength = thirdPersonLength;
	}

	public int getExtraDamage() {
		return extraDamage;
	}

	public void setExtraDamage(int extraDamage) {
		this.extraDamage = extraDamage;
	}

	public int getExtraAccuracy() {
		return extraAccuracy;
	}

	public void setExtraAccuracy(int extraAccuracy) {
		this.extraAccuracy = extraAccuracy;
	}

	public int getExtraRange() {
		return extraRange;
	}

	public void setExtraRange(int extraRange) {
		this.extraRange = extraRange;
	}

	public int getExtraFireRate() {
		return extraFireRate;
	}

	public void setExtraFireRate(int extraFireRate) {
		this.extraFireRate = extraFireRate;
	}

	public int getBullets() {
		return bullets;
	}

	public void setBullets(int bullets) {
		this.bullets = bullets;
	}

	public int getBulletsTotal() {
		return bulletsTotal;
	}

	public void setBulletsTotal(int bulletsTotal) {
		this.bulletsTotal = bulletsTotal;
	}

	public boolean isReloading() {
		return reloading;
	}

	public void setReloading(boolean reloading) {
		this.reloading = reloading;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isChangedItemWhileReloading() {
		return changedItemWhileReloading;
	}

	public void setChangedItemWhileReloading(boolean changedItemWhileReloading) {
		this.changedItemWhileReloading = changedItemWhileReloading;
	}

	public Attachment getAttachment1() {
		return attachment1;
	}

	public void setAttachment1(Attachment attachment1) {
		this.attachment1 = attachment1;
	}

	public Attachment getAttachment2() {
		return attachment2;
	}

	public void setAttachment2(Attachment attachment2) {
		this.attachment2 = attachment2;
	}

	public ItemStack getDefaultSightItem() {
		return defaultSightItem;
	}

	public void setDefaultSightItem(ItemStack defaultSightItemStack) {
		this.defaultSightItem = defaultSightItemStack;
	}

	public ItemStack getBowItem() {
		return bowItem;
	}

	public void setBowItem(ItemStack bowItemStack) {
		this.bowItem = bowItemStack;
	}

	public ArrayList<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<Attachment> attachments) {
		this.attachments = attachments;
	}

	public int getGunKills() {
		return gunKills;
	}

	public void setGunKills(int gunKills) {
		this.gunKills = gunKills;
	}

}
