package me.petersoj.weapon;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.petersoj.util.Utils;

public abstract class Weapon {
	
	private WeaponPossession weaponPossession;
	private WeaponClass weaponClass;
	private String name;
	private String desciption; // To use multiple lines "-n-"
	private String jsonName;
	private boolean boughtWithRealMoney;
	private boolean defaultWeapon;
	private int price;
	private int unlockLevel;
	private ItemStack itemStackBase;

	public Weapon(WeaponClass weaponClass, String name){
		this.weaponClass = weaponClass;
		this.name = name;
	}
	
	public void setBaseLore(){
		List<String> lore = new ArrayList<String>();
		lore.add(" ");
		if(this.weaponPossession == WeaponPossession.LOCKED){
			lore.add(ChatColor.BLUE + "Unlock at level " + ChatColor.WHITE + this.unlockLevel);
			lore.add("");
		}else if(this.weaponPossession == WeaponPossession.UNBOUGHT){
			lore.add(ChatColor.YELLOW + "Price: " + ChatColor.GOLD + ChatColor.BOLD + this.price);
			if(weaponClass.getPlayerData().getCoins() >= this.price){
				lore.add("");
				lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Click to purchase!");
			}
			lore.add("");
		}
		for(String line : this.desciption.split("-n-")){
			lore.add(ChatColor.GRAY + line);
		}
		String title = ChatColor.DARK_GREEN + "" + ChatColor.BOLD + ChatColor.UNDERLINE + this.name; // The title of the itemStack
		if(this.weaponPossession == WeaponPossession.LOCKED){
			title += ChatColor.DARK_RED + "" + ChatColor.BOLD + " - LOCKED";
		}else if(this.weaponPossession == WeaponPossession.SELECTED){
			title += ChatColor.BLUE + "" + ChatColor.BOLD + " - SELECTED";
		}
		Utils.setNameAndLore(this.itemStackBase, title, lore);
	}
	
	public void setWeaponPossession(WeaponPossession weaponPossession){
		this.weaponPossession = weaponPossession;
		if(weaponPossession == WeaponPossession.LOCKED){ // TODO Locked ItemStack
			this.itemStackBase = new ItemStack(Material.IRON_AXE, 1, (short) 4);
		}
	}
	
	public WeaponPossession getWeaponPossession(){
		return weaponPossession;
	}
	
	public WeaponClass getWeaponClass() {
		return weaponClass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getJSONName() {
		return jsonName;
	}

	public void setJSONName(String jsonName) {
		this.jsonName = jsonName;
	}

	public boolean isBoughtWithRealMoney() {
		return boughtWithRealMoney;
	}

	public void setBoughtWithRealMoney(boolean boughtWithRealMoney) {
		this.boughtWithRealMoney = boughtWithRealMoney;
	}

	public boolean isDefaultWeapon() {
		return defaultWeapon;
	}

	public void setDefaultWeapon(boolean defaultWeapon) {
		this.defaultWeapon = defaultWeapon;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getUnlockLevel() {
		return unlockLevel;
	}

	public void setUnlockLevel(int unlockLevel) {
		this.unlockLevel = unlockLevel;
	}

	public ItemStack getItemStackBase() {
		return itemStackBase;
	}

	public void setItemStackBase(ItemStack itemStackBase) {
		this.itemStackBase = itemStackBase;
	}

}
