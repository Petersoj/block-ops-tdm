package me.petersoj.util;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import io.netty.util.internal.ThreadLocalRandom;

public class Utils {
	
	public static int getRandomInt(int min, int max){
		return ThreadLocalRandom.current().nextInt(min, max);
	}
	
	public static double getRandomDouble(double min, double max){
		return ThreadLocalRandom.current().nextDouble(min, max);
	}
	
	
	// http://www.gamedev.net/topic/338987-aabb---line-segment-intersection-test
	public static boolean hasIntersection(Vector start, Vector end, Vector min, Vector max) {
        Vector d = end.clone().subtract(start).multiply(0.5);
        Vector e = max.clone().subtract(min).multiply(0.5);
        Vector c = start.clone().add(d).subtract(min.clone().add(max).multiply(0.5));
        Vector ad = new Vector(Math.abs(d.getX()), Math.abs(d.getY()), Math.abs(d.getZ()));
        if (Math.abs(c.getX()) > e.getX() + ad.getX())
            return false;
        if (Math.abs(c.getY()) > e.getY() + ad.getY())
            return false;
        if (Math.abs(c.getZ()) > e.getZ() + ad.getZ())
            return false;
        if (Math.abs(d.getY() * c.getZ() - d.getZ() * c.getY()) > e.getY() * ad.getZ() + e.getZ() * ad.getY() + 0.0001f)
            return false;
        if (Math.abs(d.getZ() * c.getX() - d.getX() * c.getZ()) > e.getZ() * ad.getX() + e.getX() * ad.getZ() + 0.0001f)
            return false;
        if (Math.abs(d.getX() * c.getY() - d.getY() * c.getX()) > e.getX() * ad.getY() + e.getY() * ad.getX() + 0.0001f)
            return false;
        return true;
    }
	
	public static String getInfoString(String text){
		return ChatColor.GREEN + "" + ChatColor.BOLD + "<" + text + "> ";
	}
	
	public static String getSpaceInfoString(String info){
		String spaces = "";
		for(int i = 0; i < info.length(); i++){
			spaces += " ";
		}
		return spaces;
	}
	
	public static String getWebsiteLink(){
		return ChatColor.GREEN + "http://www.block-ops.com";
	}
	
	public static void setNameAndLore(ItemStack item, String name, List<String> list){
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(name);
		im.setLore(list);
		im.setUnbreakable(true);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(im);
	}
	
	public static void setItemStackBlank(ItemStack... itemStack){
		for(ItemStack item : itemStack){
			setNameAndLore(item, "", null);
		}
	}
	
	/** In order for changes to take effect, you need to update the inventory! */
	public static void setItemGlowing(ItemStack item, Inventory inv, int slot, boolean glow){
		if(glow){
			item.addUnsafeEnchantment(Enchantment.DIG_SPEED, 1);
		}else{
			item.removeEnchantment(Enchantment.DIG_SPEED);
		}
		if(inv != null){
			inv.setItem(slot, item);
		}
	}
}