package me.petersoj.player;

import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.petersoj.data.PlayerData;
import me.petersoj.view.ViewController;

public class LobbyPlayer extends BlockOpsPlayer {
	
	private ViewController viewController;

	public LobbyPlayer(PlayerData playerData) {
		super(playerData);
		viewController = new ViewController(playerData);
	}
	
	@Override
	public void destroy(){
		
	}
	
	@Override
	public void setupPlayer() {
		
	}

	@Override
	public void giveSpawnItems() {
		
	}

	@Override
	public void onClick(PlayerInteractEvent e){
		
	}
	
	@Override
	public void onChat(AsyncPlayerChatEvent e) {
		
	}
	

	public ViewController getViewController(){
		return this.viewController;
	}

	




}
