package me.petersoj.player;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import me.petersoj.BlockOpsTDM;
import me.petersoj.data.PlayerData;
import me.petersoj.view.hud.HudController;
import me.petersoj.weapon.Weapon;
import me.petersoj.weapon.WeaponClass;
import me.petersoj.weapon.WeaponPossession;
import me.petersoj.weapon.attachment.Attachment;
import me.petersoj.weapon.grenade.Grenade;
import me.petersoj.weapon.gun.Gun;
import me.petersoj.weapon.perk.Perk;
import net.minecraft.server.v1_11_R1.DataWatcher;
import net.minecraft.server.v1_11_R1.DataWatcherObject;
import net.minecraft.server.v1_11_R1.DataWatcherRegistry;
import net.minecraft.server.v1_11_R1.PacketPlayOutAnimation;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityMetadata;

public class GamePlayer extends BlockOpsPlayer {
	
	private boolean dead, canShoot, sneaking;
	private Weapon currentWeapon;

	private HudController hudController;
	
	private BukkitTask sprintingCheckTask;
	
	private Vector minBodyHitbox, maxBodyHitbox, minHeadHitbox, maxHeadHitbox;
	private DataWatcher dataWatcher;
	
	public GamePlayer(PlayerData playerData) {
		super(playerData);
		
		this.hudController = new HudController(this);
		
		this.minBodyHitbox = new Vector(-0.4, 0, -0.4);
		this.maxBodyHitbox = new Vector(0.4, 1.5, 0.4); // TODO Test to make sure
		this.minHeadHitbox = new Vector(-0.2, 1.5, -0.2);
		this.maxHeadHitbox = new Vector(0.2, 1.62, 0.2);
		
		this.dataWatcher = new DataWatcher(super.getEntityPlayer());
		dataWatcher.register(new DataWatcherObject<>(6, DataWatcherRegistry.a), 1);
	}
	
	@Override
	public void setupPlayer() {
		this.sprintingCheckTask = new BukkitRunnable() { // For sprinting
			@Override
			public void run() {
				Player player = getPlayer();
				if(player.isSprinting()){
					player.setFoodLevel(player.getFoodLevel() - 2);
				}else if(player.getFoodLevel() < 8){
					player.setFoodLevel(player.getFoodLevel() + 2);
				}
			}
		}.runTaskTimer(BlockOpsTDM.getInstance(), 0, 20);
	}
	
	@Override
	public void destroy(){
		this.sprintingCheckTask.cancel();
	}

	@Override
	public void giveSpawnItems() {
		super.getPlayer().getInventory().clear();
	}
	
	@Override
	public void onClick(PlayerInteractEvent e){
		
	}
	
	public void onItemHandChange(PlayerItemHeldEvent e){
		
	}
	
	public void damage(GamePlayer damager, Weapon damagerWeapon, int hearts, boolean shakeScreen){
		int playerHealthDamaged = (int) super.getPlayer().getHealth() - hearts;
		if(playerHealthDamaged <= 0){
			this.die(damager, damagerWeapon);
			return;
		}
		super.getPlayer().setHealth(playerHealthDamaged);
		if(damager != null){
			this.hudController.getRedTint().setTint(playerHealthDamaged);
			this.hudController.createDamageIndicator(damager);
		}
		if(shakeScreen){
			this.shakeScreen();
		}
	}
	
	public void die(GamePlayer killer, Weapon damagerWeapon){
		
		// call GameController.onDeath();
		this.hudController.getRedTint().resetTint(20);
	}
	
	
	
	public void updateWeaponLores(){
		int playerLevel = (int) super.getPlayerData().getPlayerLevel(true);
		for(WeaponClass weaponClass : super.getPlayerData().getWeaponClasses()){
			for(Gun gun : weaponClass.getGuns()){ // Guns
				if(playerLevel > gun.getUnlockLevel() && gun.getWeaponPossession() == WeaponPossession.LOCKED){ // for Locked guns
					gun.setWeaponPossession(WeaponPossession.UNBOUGHT);
				}else{
					gun.setWeaponPossession(gun.getWeaponPossession()); // reset's lore for Class selection gui's
				}
				float gunLevel = gun.getGunLevel();
				for(Attachment attachment : gun.getAttachments()){ // For locked Attachments
					if(gunLevel > attachment.getUnlockLevel() && attachment.getWeaponPossession() == WeaponPossession.LOCKED){
						attachment.setWeaponPossession(WeaponPossession.UNBOUGHT);
					}else{
						attachment.setWeaponPossession(attachment.getWeaponPossession()); // reset's lore for Class selection gui's
					}
				}
			}
			for(Grenade grenade : weaponClass.getGrenades()){
				if(playerLevel > grenade.getUnlockLevel() && grenade.getWeaponPossession() == WeaponPossession.LOCKED){ // for Locked grenades
					grenade.setWeaponPossession(WeaponPossession.UNBOUGHT);
				}else{
					grenade.setWeaponPossession(grenade.getWeaponPossession()); // reset's lore for Class selection gui's
				}
			}
			for(Perk perk : weaponClass.getPerks()){
				if(playerLevel > perk.getUnlockLevel() && perk.getWeaponPossession() == WeaponPossession.LOCKED){ // for Locked perks
					perk.setWeaponPossession(WeaponPossession.UNBOUGHT);
				}
			}
		}
	}
	
	
	public void shakeScreen(){
		PacketPlayOutAnimation animationPacket = new PacketPlayOutAnimation(super.getEntityPlayer(), 1);
		super.sendPacket(animationPacket);
	}
	
	public void setAiming(boolean aiming){
		if(aiming){
			dataWatcher.set(new DataWatcherObject<>(6, DataWatcherRegistry.a), (byte) 1);
		}else{
			dataWatcher.set(new DataWatcherObject<>(6, DataWatcherRegistry.a), (byte) 0);
		}
		for(GamePlayer gamePlayer : super.getPlayerData().getTDMController().getPlayerController().getGamePlayers()){
			if(!gamePlayer.equals(this)){
				gamePlayer.sendPacket(new PacketPlayOutEntityMetadata(super.getEntityPlayer().getId(), this.dataWatcher, true));
			}
		}
	}

	
	public Weapon getCurrentWeapon() {
		return currentWeapon;
	}

	public void setCurrentWeapon(Weapon currentWeapon) {
		this.currentWeapon = currentWeapon;
	}

	public HudController getHudController() {
		return hudController;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public boolean canShoot() {
		return canShoot;
	}

	public void setCanShoot(boolean canShoot) {
		this.canShoot = canShoot;
	}

	public boolean isSneaking() {
		return sneaking;
	}

	public void setSneaking(boolean sneaking) {
		this.sneaking = sneaking;
	}

	public Vector getMinBodyHitbox() {
		return minBodyHitbox;
	}

	public Vector getMaxBodyHitbox() {
		return maxBodyHitbox;
	}

	public Vector getMinHeadHitbox() {
		return minHeadHitbox;
	}

	public Vector getMaxHeadHitbox() {
		return maxHeadHitbox;
	}
}