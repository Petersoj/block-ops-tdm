package me.petersoj.player;

import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.petersoj.data.PlayerData;
import me.petersoj.view.hud.ScoreBoard;
import me.petersoj.view.hud.Titles;
import me.petersoj.weapon.WeaponClass;
import net.minecraft.server.v1_11_R1.EntityPlayer;
import net.minecraft.server.v1_11_R1.Packet;

public abstract class BlockOpsPlayer {
	
	private PlayerData playerData;
	private EntityPlayer entityPlayer;

	private ScoreBoard scoreBoard;
	private Titles titles;
	
	private WeaponClass selectedClass;
	
	public BlockOpsPlayer(PlayerData playerData){
		this.playerData = playerData;
		this.entityPlayer = ((CraftPlayer)playerData.getPlayer()).getHandle();
		
		this.scoreBoard = new ScoreBoard(this);
		this.titles = new Titles(this);
	}
	
	public abstract void setupPlayer();
	
	public abstract void destroy();
	
	public abstract void giveSpawnItems();
	
	public abstract void onClick(PlayerInteractEvent e);
	
	public void onChat(AsyncPlayerChatEvent e){
		
	}
	
	public void sendPacket(Packet<?> packet){
		entityPlayer.playerConnection.sendPacket(packet);
	}
	
	public Player getPlayer(){
		return playerData.getPlayer();
	}
	
	public EntityPlayer getEntityPlayer() {
		return entityPlayer;
	}
	
	public ScoreBoard getScoreBoard(){
		return scoreBoard;
	}
	
	public Titles getTitles(){
		return titles;
	}

	public WeaponClass getSelectedClass() {
		return selectedClass;
	}

	public void setSelectedClass(WeaponClass selectedClass) {
		this.selectedClass = selectedClass;
	}

	public PlayerData getPlayerData() {
		return playerData;
	}
}
