package me.petersoj.game;

import me.petersoj.data.PlayerController;
import me.petersoj.data.database.JedisController;
import me.petersoj.listener.Listeners;

public class TDMController {
	
	
	private PlayerController playerController;
	private JedisController jedisController;
	
	private Listeners listeners;
	
	private GameController gameController;
	private LobbyController lobbyController;
	
	public TDMController(){
		this.playerController = new PlayerController(this);
		this.jedisController = new JedisController(this);
		
		this.listeners = new Listeners(this);
	}
	
	public boolean isGameInSession(){
		return gameController != null;
	}

	public void setGameController(){
		this.gameController = new GameController(this);
	}
	
	public void setLobbyController(){
		this.lobbyController =  new LobbyController(this);
	}
	
	public PlayerController getPlayerController() {
		return playerController;
	}

	public JedisController getJedisController() {
		return jedisController;
	}

	public GameController getGameController() {
		return gameController;
	}

	public LobbyController getLobbyController() {
		return lobbyController;
	}
	
}
