package me.petersoj.game;

import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.petersoj.data.PlayerData;
import me.petersoj.world.BlockOpsMap;

public class GameController {
	
	private TDMController tdmController;
	
	private boolean gameInProgress;
	private BlockOpsMap currentMap;
	
	


	// This Class will handle all the methods for when the Game is in progress.
	public GameController(TDMController tdmController){
		this.tdmController = tdmController;
	}
	
	
	public void onJoin(PlayerJoinEvent e){
		
		this.tdmController.getPlayerController().fetchPlayerData(e.getPlayer());
		
		// put them in a place to spawn
	}
	
	public void onQuit(PlayerQuitEvent e){
		this.tdmController.getPlayerController().pushPlayerData(e.getPlayer());
	}

	public void onPlayerDataFetched(PlayerData playerData){
		// If they join while the gameInProgress, then the become a spectator player
	}
	
	public void onPlayerDataPushed(PlayerData playerData){
		
	}


	

	public TDMController getTDMController() {
		return tdmController;
	}
	
	public BlockOpsMap getCurrentMap() {
		return currentMap;
	}
	
	public boolean isGameInProgress() {
		return gameInProgress;
	}
	
	
}
