package me.petersoj.game;

import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.petersoj.data.PlayerData;

public class LobbyController {
	
	private TDMController tdmController;

	// This Class will handle all the methods for when the lobby is in progress.
	public LobbyController(TDMController tdmController){
		this.tdmController = tdmController;
	}
	
	
	public void onJoin(PlayerJoinEvent e){
		this.tdmController.getPlayerController().fetchPlayerData(e.getPlayer());
		// put them in a place to spawn
	}
	
	public void onQuit(PlayerQuitEvent e){
		this.tdmController.getPlayerController().pushPlayerData(e.getPlayer());
	}

	public void onPlayerDataFetched(PlayerData playerData){
		tdmController.getPlayerController().addPlayerData(playerData.getPlayer().getUniqueId(), playerData);
		Bukkit.broadcastMessage(playerData.getRank().toString());
		Bukkit.broadcastMessage(String.valueOf(playerData.getCoins()));
	}
	
	public void onPlayerDataPushed(PlayerData playerData){
		
	}
	

	public TDMController getTDMController() {
		return tdmController;
	}
	
	
}
